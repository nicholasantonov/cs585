#include "gnomelevel.hh"

#include "i-actor.hh"
#include "scene-manager.hh"
#include "fixed-grid.hh"
#include "ncurses-input.hh"
#include "event-bus.hh"
#include "input-event.hh"
#include "i-state.hh"
#include "trie.hh"
#include "cursor-state.hh"
#include "camera-state.hh"
#include "state-machine.hh"
#include "input-manager.hh"
#include "interact-state.hh"
#include "ascii-renderer.hh"
#include "json.hh"
#include "actor-factory.hh"
#include "i-event.hh"
#include "death-event.hh"
#include "debug.hh"
#include "to-string.hh"
#include "orc-spawner.hh"

#define DEATH_MESSAGE_TIME 0.8f

/* Gnome level because you are the bureaucratic gnome that manages the fortress
 * definitely not because I originally made a typo
 */
void GnomeLevel::loadLevel(std::string directory)
{

	DEBUG_LOG("level", "Getting config files");
	Json* files = Json::parseFile(directory + "config.json");
	Json* world = Json::parseFile(directory + files->getMember("world")->getString());
	Json* menuConfig = Json::parseFile(directory + files->getMember("menus")->getString());

	DEBUG_LOG("level", "setting initial parameters");
	gold = (int)(world->getMember("gold")->getNumber());

	DEBUG_LOG("level", "Setting scenegraph");
	int x = (int)(world->getMember("xsize")->getNumber());
	int y = (int)(world->getMember("ysize")->getNumber());
	SceneManager::getInstance()->setSceneGraph(new FixedGrid(x, y));

	DEBUG_LOG("level", "Building menus");
	Trie<DynamicArray<std::string>*>* menus = new Trie<DynamicArray<std::string>*>();
	menus->insert("dwarf", getMenu(menuConfig, "dwarf"));
	menus->insert("grandhall", getMenu(menuConfig, "grandhall"));
	menus->insert("apothecary", getMenu(menuConfig, "apothecary"));
	menus->insert("blacksmith", getMenu(menuConfig, "blacksmith"));

	DEBUG_LOG("level", "Setting game states");
	Trie<IState*>* stateMap = new Trie<IState*>();

	CameraState* camera = new CameraState();
	stateMap->insert("camera", camera);

	CursorState* cursor = new CursorState();
	cursor->levelSelectedActor = &selectedActor;
	stateMap->insert("cursor", cursor);
	
	InteractState* interact = new InteractState(menus);
	interact->levelSelectedActor = &selectedActor;
	stateMap->insert("interact", interact);

	stateMachine = new StateMachine(stateMap, "camera", (IActor*)nullptr);

	DEBUG_LOG("level", "Populating the world");
	factory = new ActorFactory(directory);

	Json* objects = Json::parseFile(directory + files->getMember("objects")->getString());
	
	DynamicArray<Json*>* dwarves = objects->getMember("dwarves")->getArray();
	for (unsigned int i = 0; i < dwarves->length(); i++)
	{
		int xpos = dwarves->get(i)->getMember("x")->getNumber();
		int ypos = dwarves->get(i)->getMember("y")->getNumber();
		factory->createDwarf(xpos, ypos);
	}

	DynamicArray<Json*>* orcs = objects->getMember("orcs")->getArray();
	for (unsigned int i = 0; i < orcs->length(); i++)
	{
		int xpos = orcs->get(i)->getMember("x")->getNumber();
		int ypos = orcs->get(i)->getMember("y")->getNumber();
		factory->createOrc(xpos, ypos);
	}

	DynamicArray<Json*>* spawners = objects->getMember("orcspawners")->getArray();
	float spawnDelay = Json::parseFile(directory + files->getMember("spawnconfig")->getString())->getMember("delay")->getNumber();
	for (unsigned int i = 0; i < spawners->length(); i++)
	{
		int xpos = spawners->get(i)->getMember("x")->getNumber();
		int ypos = spawners->get(i)->getMember("y")->getNumber();
		SceneManager::getInstance()->addTickable(new OrcSpawner(xpos, ypos, spawnDelay, factory));
	}

	DynamicArray<Json*>* trees = objects->getMember("trees")->getArray();
	for (unsigned int i = 0; i < trees->length(); i++)
	{
		int xpos = trees->get(i)->getMember("x")->getNumber();
		int ypos = trees->get(i)->getMember("y")->getNumber();
		factory->createTree(xpos, ypos);
	}

	DynamicArray<Json*>* mountains = objects->getMember("mountains")->getArray();
	for (unsigned int i = 0; i < mountains->length(); i++)
	{
		int xpos = mountains->get(i)->getMember("x")->getNumber();
		int ypos = mountains->get(i)->getMember("y")->getNumber();
		factory->createMountain(xpos, ypos);
	}

	int grandHallXPos = objects->getMember("grandhall")->getMember("x")->getNumber();
	int grandHallYPos = objects->getMember("grandhall")->getMember("y")->getNumber();
	factory->createGrandHall(grandHallXPos, grandHallYPos);

	listener.setInstance(this);

	DEBUG_LOG("level", "Setting Renderer and inputs");
	SceneManager::getInstance()->setRenderer(new AsciiRenderer());
	SceneManager::getInstance()->moveCamera(0, 0);
	InputManager::getInstance()->setInputHandler(new NcursesInput());
}

GnomeLevel::~GnomeLevel()
{

}

void GnomeLevel::tick(float dt)
{
	InputManager::getInstance()->tick(dt);
	stateMachine->tick(dt);
	SceneManager::getInstance()->render(dt);
}

DynamicArray<std::string>* GnomeLevel::getMenu(Json* json, std::string type)
{
	DynamicArray<Json*>* jsonItems = json->getMember(type)->getArray();
	DynamicArray<std::string>* items = new DynamicArray<std::string>();
	for (unsigned int i = 0; i < jsonItems->length(); i++)
	{
		items->push(jsonItems->get(i)->getString());
	}
	return items;
}

int GnomeLevel::getGold()
{
	return gold;
}

void GnomeLevel::addGold(int g)
{
	gold += g;
}

IListener* GnomeLevel::getListener()
{
	return &listener;
}

void GnomeLevel::LevelListener::setInstance(GnomeLevel* value)
{
	instance = value;
}

void GnomeLevel::LevelListener::handle(IEvent* e)
{
	if (e->getType().compare(DEATH_EVENT_TYPE) == 0)
	{
		DeathEvent* ev = (DeathEvent*)e;
		
		if (ev->killer != nullptr)
		{
			ev->killer->attributes->get("gold") += ev->dier->attributes->get("gold");
		}
		SceneManager::getInstance()->displayMessage(ev->dier->getType() + toString(ev->dier->id) + " has just died!", DEATH_MESSAGE_TIME);
		
		instance->factory->deleteCharacter(ev->dier);
	}
}