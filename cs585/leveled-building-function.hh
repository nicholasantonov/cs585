#ifndef _LEVELED_BUILDING_FUNCTION_HH_
#define _LEVELED_BUILDING_FUNCTION_HH_

#include "i-function.hh"
#include "building.hh"
#include <string>

class LeveledBuildingFunction : public IFunction
{
public:
	LeveledBuildingFunction(Building* b);
	void handleInput(int input, int inputX, int inputY);
private:
	Building* building;
};
#endif