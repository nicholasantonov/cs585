#ifndef _DEATH_EVENT_HH
#define _DEATH_EVENT_HH

#define DEATH_EVENT_TYPE "death"

#include "i-event.hh"

#include "character.hh"
#include <string>

// Event that gets dispatched to tell the level that an actor died, possibly to another actor
class DeathEvent : public IEvent
{
public:
	DeathEvent(Character* dead, Character* kill);
	std::string getType();
	Character* dier;
	Character* killer;
};

#endif