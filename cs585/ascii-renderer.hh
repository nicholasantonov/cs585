#ifndef _ASCII_RENDERER_HH_
#define _ASCII_RENDERER_HH_

#include "i-renderer.hh"
#include "i-drawable.hh"
#include "dynamic-array.hh"
#include "trie.hh"

class AsciiRenderer : public IRenderer
{
public:
	AsciiRenderer();
	~AsciiRenderer();
	void tick(float dt);

	void setCamera(int start_x, int stary_y);
	void moveCursor(int x, int y);
	void hideCursor();
	void showCursor();
	void displayMessage(std::string, float);
	void displayMenu(DynamicArray<std::string>*);
	void closeMenu();
	void displayStatus(std::string);
	void closeStatus();
private:
	void renderScene();
	void renderMessage(float dt);
	void renderStatus();
	void renderMenu();
	void draw(int x, int y, IDrawable* d);

	Trie<int>* colorMap;
	float messageTimeRemaining;
	std::string alertString;
	std::string status;

	DynamicArray<std::string>* menuItems;
	unsigned int menuWidth;

	int render_x;
	int render_y;
};

#endif