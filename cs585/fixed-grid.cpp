#include "fixed-grid.hh"

#include "debug.hh"
#include "i-scene-graph.hh"
#include "scene-node.hh"
#include "dynamic-array.hh"
#include "i-drawable.hh"

FixedGrid::FixedGrid(int x, int y) :
	xdim(x),
	ydim(y),
	positions(new DynamicArray<SceneNode*>*[(unsigned int)(xdim * ydim)])
{
	for (int i = 0; i < (xdim * ydim); i++)
	{
		positions[i] = new DynamicArray<SceneNode*>(4);
	}
}

FixedGrid::~FixedGrid()
{
	delete[] positions;
}

DynamicArray<SceneNode*>* FixedGrid::nodesAt(int x, int y)
{
	return positions[(x * ydim) + y];
}

void FixedGrid::addSceneNode(SceneNode* node)
{
	DEBUG_LOG("fixedgrid", "Adding node");
	nodesAt(node->getX(), node->getY())->push(node);
}

void FixedGrid::removeSceneNode(SceneNode* node)
{
	DynamicArray<SceneNode*>* containingLocation = nodesAt(node->getX(), node->getY());
	for (unsigned int i = 0; i < containingLocation->length(); i++)
	{
		if (containingLocation->get(i) == node)
		{
			DEBUG_LOG("fixedgrid", "removing node");
			containingLocation->remove(i);
			return;
		}
	}
}

void FixedGrid::updateSceneNode(SceneNode* node, int x, int y)
{
	DEBUG_LOG("fixedgrid", "Updating node");
	removeSceneNode(node);
	node->update(x, y);
	addSceneNode(node);
}

DynamicArray<SceneNode*>* FixedGrid::getColliders(SceneNode* node)
{
	DynamicArray<SceneNode*>* colliders = new DynamicArray<SceneNode*>(4);
	DynamicArray<SceneNode*>* containingLocation = nodesAt(node->getX(), node->getY());

	for (unsigned int i = 0; i < containingLocation->length(); i++)
	{
		if (node->collisions() & containingLocation->get(i)->collisions())
		{
			colliders->push(containingLocation->get(i));
		}
	}

	return colliders;
}

DynamicArray<SceneNode*>* FixedGrid::getColliders(int x, int y)
{
	return nodesAt(x, y);
}

DynamicArray<SceneNode*>* FixedGrid::getColliders(int x, int y, int radius)
{
	int minX = (x - radius) >= 0 ? x - radius : 0;
	int minY = (y - radius) >= 0 ? y - radius : 0;
	int maxX = (x + radius) < xdim ? x + radius : xdim - 1;
	int maxY = (y + radius) < ydim ? y + radius : ydim - 1;

	DynamicArray<SceneNode*>* looseCollisions = getColliders(minX, minY, maxX, maxY);
	DynamicArray<SceneNode*>* collisions = new DynamicArray<SceneNode*>();

	for (unsigned int i = 0; i < looseCollisions->length(); i++)
	{
		int x = looseCollisions->get(i)->getX();
		int y = looseCollisions->get(i)->getY();
		if ((x * x) + (y * y) <= (radius * radius))
		{
			collisions->push(looseCollisions->get(i));
		}
	}
	return collisions;
}

DynamicArray<SceneNode*>* FixedGrid::getColliders(int cornerX1, int cornerY1, int cornerX2, int cornerY2)
{
	DynamicArray<SceneNode*>* collisions = new DynamicArray<SceneNode*>();
	for (int i = cornerX1; i <= cornerX2 && i < xdim; i++)
	{
		for (int j = cornerY1; j <= cornerY2 && j < ydim; j++)
		{
			DynamicArray<SceneNode*> contents = *nodesAt(i, j);
			for (unsigned int k = 0; k < contents.length(); k++)
			{
				collisions->push(contents.get(k));
			}
		}
	}
	return collisions;
}

t_coordSet FixedGrid::getBounds()
{
	t_coordSet info;
	info.minX = 0;
	info.minY = 0;
	info.maxX = xdim - 1;
	info.maxY = ydim - 1;
	return info;
}

SceneNode* FixedGrid::getTopDrawableNodeAt(int x, int y)
{
	DEBUG_LOG("fixedgrid", "Getting Drawable");
	SceneNode* topNode = nullptr;
	if (x >= xdim || y >= ydim)
	{
		DEBUG_ERROR("fixedgrid", "Getting OOB Drawable");
		return nullptr;
	}
	DynamicArray<SceneNode*>* nodes = nodesAt(x, y);
	for (unsigned int i = 0; i < nodes->length(); i++)
	{
		// If the current node is drawable
		if (nodes->get(i)->drawable != nullptr)
		{
			// If we dont already have a node, or this node has a higher z index
			if (topNode == nullptr || nodes->get(i)->drawable->zIndex > topNode->drawable->zIndex)
			{
				topNode = nodes->get(i);
			}
		}
	}
	return topNode;
}

void FixedGrid::setDefaultTile(SceneNode* node)
{
	//TODO set a scenenode as default
}

bool FixedGrid::canMoveTo(SceneNode* node, int x, int y)
{
	// Check that the coordinates are in bounds
	if (x < 0 ||
		y < 0 ||
		x > xdim ||
		y > ydim)
	{
		return false;
	}
	
	// Check that the actor does not collide with anything on that tile
	DynamicArray<SceneNode*>* colliders = getColliders(x, y);
	for (unsigned int i = 0; i < colliders->length(); i++)
	{
		if (colliders->get(i)->collisions() & node->collisions())
		{
			return false;
		}
	}

	return true;
}