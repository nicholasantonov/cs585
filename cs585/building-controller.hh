#ifndef _BUILDING_CONTROLLER_HH_
#define _BUILDING_CONTROLLER_HH_

#include "i-tickable.hh"
#include "building.hh"
#include "i-actor.hh"

class BuildingController : public ITickable
{
public:
	BuildingController(Building*);
	void tick(float dt);
private:
	int startX;
	int startY;
	int currentOffset;
	int maxOccupants;

	Building* building;
	IActor** occupants;
};

#endif