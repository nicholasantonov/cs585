#ifndef _I_LISTENER_HH_
#define _I_LISTENER_HH_

#include "i-event.hh"

// Listener interface that requires listeners to implement a function to handle the event
class IListener
{
public:
	virtual void handle(IEvent*) = 0;
};

#endif