#include "event-bus.hh"

EventBus* EventBus::instance = nullptr;

EventBus* EventBus::getInstance()
{
	if (instance == nullptr)
	{
		instance = new EventBus();
	}
	return instance;
}

void EventBus::addListener(std::string eventType, IListener* listener)
{
	dispatcher->addListener(eventType, listener);
}

void EventBus::removeListener(std::string eventType, IListener* listener)
{
	dispatcher->removeListener(eventType, listener);
}

void EventBus::dispatch(IEvent* ev)
{
	dispatcher->dispatch(ev);
}

void EventBus::tick(float dt)
{
	dispatcher->tick(dt);
}

EventBus::EventBus() :
	dispatcher(new Dispatcher())
{ }