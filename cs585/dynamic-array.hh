#ifndef _DYNAMIC_ARRAY_HH_
#define _DYNAMIC_ARRAY_HH_

#include <utility>
#include "debug.hh"

template <class Elem>
class DynamicArray
{
public:
	
	// Default Constructor
	DynamicArray()
	{
		DEBUG_LOG("dynamicarray", "Default Constructing");
		numberOfElements = 0;
		capacity = 16;
		storage = new Elem[capacity];
	}

	// Constructor where user can specify an initial capacity
	DynamicArray(unsigned int initialCap)
	{
		DEBUG_LOG("dynamicarray", "Constructing with given size");
		numberOfElements = 0;
		capacity = initialCap;
		storage = new Elem[capacity];
	}

	// Destructor
	~DynamicArray()
	{
		DEBUG_LOG("dynamicarray", "Deconstructing");
		delete [] storage;
	}

	// Returns the length of the array
	unsigned int length()
	{
		DEBUG_LOG("dynamicarray", "getting length");
		return numberOfElements;
	}

	// Returns the current number of allocated memory locations for the dynamic array
	unsigned int allocated()
	{
		DEBUG_LOG("dynamicarray", "Getting capacity");
		return capacity;
	}

	// Returns the element at the given index
	Elem get(unsigned int index)
	{
		DEBUG_LOG("dynamicarray", "Getting an element");
		return storage[index];
	}

	// Adds a new element at the end of the array and return it's index
	unsigned int push(Elem element)
	{
		DEBUG_LOG("dynamicarray", "pushing an element");
		insert(element, numberOfElements);
		return (numberOfElements - 1);
	}

	// Adds a new element at the beginning of the array
	void unshift(Elem element)
	{
		DEBUG_LOG("dynamicarray", "unshifting an element");
		insert(element, 0);
	}

	// Adds a new element at the provided index
	void insert(Elem element, unsigned int index)
	{
		DEBUG_LOG("dynamicarray", "inserting an element");
		if (numberOfElements == capacity)
		{
			DEBUG_WARN("dynamicarray", "insert causing array resize");
			capacity = calculateNextCapacity();
			Elem* newStorage = new Elem[capacity];
			for (unsigned int i = 0; i < index; i++)
			{
				newStorage[i] = std::move(storage[i]);
			}
			for (unsigned int i = index; i < numberOfElements; i++)
			{
				newStorage[i + 1] = std::move(storage[i]);
			}
			delete storage;
			storage = newStorage;
		}
		else
		{
			for (unsigned int i = numberOfElements; i > index; i--)
			{
				storage[i] = std::move(storage[i - 1]);
			}
		}
		storage[index] = element;
		numberOfElements++;

	}

	// Swaps the elements at the two indices provided
	void swap(unsigned int a, unsigned int b)
	{
		DEBUG_LOG("dynamicarray", "swapping elements");
		if (a < numberOfElements && b < numberOfElements)
		{
			Elem temp = storage[a];
			storage[a] = storage[b];
			storage[b] = temp;
		}
	}

	// Changes the element at a particular index
	void set(Elem element, unsigned int index)
	{
		DEBUG_LOG("dynamicarray", "setting an element");
		storage[index] = element;
	}

	// Increases the number of allocated positions by the number given
	void allocate(unsigned int newSpaces)
	{
		DEBUG_LOG("dynamicarray", "manually allocating new space");
		capacity += newSpaces;
		Elem* newStorage = new Elem[capacity];
		for (unsigned int i = 0; i < numberOfElements; i++)
		{
			newStorage[i] = std::move(storage[i]);
		}
		delete storage;
		storage = newStorage;
	}

	// Delete an item at a given index, and move the rest of the array up to fill its place
	Elem remove(unsigned int index)
	{
		DEBUG_LOG("dynamicarray", "removing an element");
		Elem element = 0;
		if (index < numberOfElements)
		{
			element = storage[index];
			numberOfElements--;
			for (unsigned int i = index; i < numberOfElements; i++)
			{
				storage[i] = storage[i + 1];
			}
		}
		return element;
	}

	void removeElement(Elem e)
	{
		DEBUG_LOG("dynamicarray", "removing an element");
		for (int i = 0; i < numberOfElements; i++)
		{
			if (get(i) == e)
			{
				remove(i);
			}
		}
	}

protected:
	Elem* storage;
	unsigned int numberOfElements;
	unsigned int capacity;

private:
	// Return what size the dynamic array should grow to
	unsigned int calculateNextCapacity()
	{
		return (capacity < 2) ? 2 : (unsigned int)(capacity * 1.5);
	}
};

#endif