#include "building-controller.hh"

#include "scene-manager.hh"
#include "i-actor.hh"


BuildingController::BuildingController(Building* b) :
	startX(b->getX() + 1),
	startY(b->getY() + 1),
	currentOffset(0),
	maxOccupants(b->size * 2),
	building(b),
	occupants(new IActor*[b->size * 2])
{
	for (int i = 0; i < (maxOccupants); i++)
	{
		occupants[i] = nullptr;
	}
}

void BuildingController::tick(float dt)
{
	int x = (int)building->entranceX;
	int y = (int)building->entranceY;
	IActor* actor = (IActor*)SceneManager::getInstance()->getSceneGraph()->getTopDrawableNodeAt(x, y);
	
	// Make sure a dwarf is at the door	
	if (actor != nullptr && actor->attributes->get("enteringbuilding"))
	{
		
		// Update next placement position
		for (int i = 0; i < maxOccupants; i++)
		{
			if (occupants[i] == nullptr)
			{
				SceneManager::getInstance()->getSceneGraph()->updateSceneNode(actor, startX + (i % building->size), startY + (i / building->size));
				break;
			}
		}
	}
}