#include "ascii-renderer.hh"

#include "scene-manager.hh"
#include "i-scene-graph.hh"
#include "i-drawable.hh"
#include "ascii-drawable.hh"
#include "debug.hh"
#include "trie.hh"
#include <ncurses.h>
#include <stdio.h>
#include <ctime>
#include <iomanip>
#include <cstdlib>

#define CURSOR_BLINK_DELAY 500
#define DEFAULT_RENDER_CHAR '.'
#define MENU_LEFT_BORDER (char)186
#define MENU_CORNER (char)200
#define MENU_BOTTOM_BORDER (char)205

#define PAIR_RED     1
#define PAIR_GREEN   2
#define PAIR_YELLOW  3
#define PAIR_BLUE    4
#define PAIR_MAGENTA 5
#define PAIR_CYAN    6
#define PAIR_WHITE   7

AsciiRenderer::AsciiRenderer()
{
	initscr();
	noecho(); // Don't show input
	cbreak();
	keypad(stdscr, TRUE);
	nodelay(stdscr, TRUE); // reading input won't wait for enter
	messageTimeRemaining = 0;
	menuItems = nullptr;
	status = "";

	//set up color pairs for the default colors
	colorMap = new Trie<int>();
	init_pair(PAIR_RED,		COLOR_RED,		COLOR_BLACK);
	colorMap->insert("red", PAIR_RED);

	init_pair(PAIR_GREEN,	COLOR_GREEN,	COLOR_BLACK);
	colorMap->insert("green", PAIR_GREEN);

	init_pair(PAIR_YELLOW,	COLOR_YELLOW,	COLOR_BLACK);
	colorMap->insert("yellow", PAIR_YELLOW);

	init_pair(PAIR_BLUE,	COLOR_BLUE,		COLOR_BLACK);
	colorMap->insert("blue", PAIR_BLUE);

	init_pair(PAIR_MAGENTA,	COLOR_MAGENTA,	COLOR_BLACK);
	colorMap->insert("magenta",	PAIR_MAGENTA);

	init_pair(PAIR_CYAN,	COLOR_CYAN,		COLOR_BLACK);
	colorMap->insert("cyan", PAIR_CYAN);

	init_pair(PAIR_WHITE,	COLOR_WHITE,	COLOR_BLACK);
	colorMap->insert("white", PAIR_WHITE);
}

AsciiRenderer::~AsciiRenderer()
{
	endwin();
}

void AsciiRenderer::tick(float dt)
{
	renderScene();
	renderStatus();
	renderMessage(dt);
	renderMenu();
	refresh();
}

// Sets the position of top left-most coordinate to be rendered
void AsciiRenderer::setCamera(int start_x, int start_y)
{
	render_x = start_x;
	render_y = start_y;
}

void AsciiRenderer::renderScene()
{
	// Save the cursors position to replace cursor after rendering
	int cursor_x;
	int cursor_y;
	getyx(curscr, cursor_y, cursor_x);

	// Set up start
	int startWorld_x = render_x;
	int startWorld_y = render_y;

	// Get max bounds of game world
	int world_x;
	int world_y;
	int maxWorld_x;
	int maxWorld_y;
	ISceneGraph* sceneGraph = SceneManager::getInstance()->getSceneGraph();
	t_coordSet bounds = sceneGraph->getBounds();
	maxWorld_x = bounds.maxX;
	maxWorld_y = bounds.maxY;

	// Get bounds of the screen
	int screenEnd_x;
	int screenEnd_y;
	getmaxyx(stdscr, screenEnd_y, screenEnd_x);
	
	int x;
	int y;
	erase();
	//Each loop checks that the current position is both in the bounds of the console screen and the world
	for (x = 0, world_x = startWorld_x; (world_x <= maxWorld_x) && (x <= screenEnd_x); x++, world_x++)
	{
		for (y = 0, world_y = startWorld_y; (world_y <= maxWorld_y) && (y <= screenEnd_y); y++, world_y++)
		{
			SceneNode* node = sceneGraph->getTopDrawableNodeAt(world_x, world_y);

			if (node == nullptr)
			{
				draw(x, y, nullptr);
			}
			else
			{
				draw(x, y, node->drawable);
			}
		}
	}

	// Replace cursor to initial location
	move(cursor_y, cursor_x);
}

void AsciiRenderer::hideCursor()
{
	curs_set(FALSE); // Don't show cursor
}

void AsciiRenderer::showCursor()
{
	curs_set(TRUE); // Show cursor
}

// Moves the cursor to the given X and Y with respect to worldspace, not screenspace
void AsciiRenderer::moveCursor(int x, int y)
{
	move(y - render_y, x - render_x);
}

// Draw the given drawable at the given coordinatess
void AsciiRenderer::draw(int x, int y, IDrawable* drawable)
{
	AsciiDrawable* asciiDrawable = (AsciiDrawable*)drawable;
	char character = DEFAULT_RENDER_CHAR;
	if (asciiDrawable != nullptr)
	{
		character = asciiDrawable->icon;
	}
	mvaddch(y, x, character);
}

void AsciiRenderer::renderMessage(float dt)
{
	if (messageTimeRemaining > 0)
	{
		messageTimeRemaining -= dt;
		// Save the cursors position to replace cursor after rendering
		int cursor_x;
		int cursor_y;
		getyx(curscr, cursor_y, cursor_x);

		// Get the last line number
		int max_x;
		int max_y;
		getmaxyx(stdscr, max_y, max_x);

		// Blank out the bottom line
		move(max_y - 1, 0);
		clrtoeol();

		printw(alertString.c_str());

		// Replace cursor to initial location
		move(cursor_y, cursor_x);
	}
}

void AsciiRenderer::displayMessage(std::string s, float timeToDisplayFor)
{
	messageTimeRemaining = timeToDisplayFor;
	alertString = s;
}

void AsciiRenderer::renderMenu()
{
	if (menuItems != nullptr)
	{
		// Save the cursors position to replace cursor after rendering
		int cursor_x;
		int cursor_y;
		getyx(curscr, cursor_y, cursor_x);

		// Get the last line number
		unsigned int max_x;
		unsigned int max_y;
		getmaxyx(stdscr, max_y, max_x);

		unsigned int itemCount = menuItems->length();
		unsigned int menuStart = max_x - menuWidth - 3; // 1 for the  border, 2 for the spaces

		unsigned int y;
		//Print items to menu
		for (y = 0; y < itemCount && y < max_y; y++)
		{
			move(y, menuStart);
			clrtoeol();
			addch(MENU_LEFT_BORDER);
			mvprintw(y, menuStart + 2, menuItems->get(y).c_str());
		}
		for (; y < max_y; y++)
		{
			move(y, menuStart);
			clrtoeol();
			addch(MENU_LEFT_BORDER);
		}


		// Replace cursor to initial location
		move(cursor_y, cursor_x);
	}
}

void AsciiRenderer::displayMenu(DynamicArray<std::string>* items)
{
	menuItems = items;
	menuWidth = 0;
	for (unsigned int i = 0, numOfItems = menuItems->length(); i < numOfItems; i++)
	{
		if (menuItems->get(i).length() > menuWidth)
		{
			menuWidth = menuItems->get(i).length();
		}
	}
}

void AsciiRenderer::closeMenu()
{
	menuItems = nullptr;
}

void AsciiRenderer::displayStatus(std::string s)
{
	DEBUG_LOG("asciirenderer", "setting status to " + s);
	status = s;
}

void AsciiRenderer::closeStatus()
{
	status = "";
}

void AsciiRenderer::renderStatus()
{
	if (status.length() > 0)
	{
		// Save the cursors position to replace cursor after rendering
		int cursor_x;
		int cursor_y;
		getyx(curscr, cursor_y, cursor_x);

		// Get the last line number
		int max_x;
		int max_y;
		getmaxyx(stdscr, max_y, max_x);

		// Blank out the bottom line
		move(max_y - 1, 0);
		clrtoeol();

		printw(status.c_str());

		// Replace cursor to initial location
		move(cursor_y, cursor_x);
	}
}