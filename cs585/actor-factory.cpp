#include "actor-factory.hh"

#include "json.hh"
#include "character.hh"
#include "ascii-drawable.hh"
#include "scene-manager.hh"
#include "state-machine.hh"
#include "brownian-state.hh"
#include "move-state.hh"
#include "trie.hh"
#include "i-state.hh"
#include "i-actor.hh"
#include "terrain.hh"
#include "dispatcher.hh"
#include "building.hh"
#include "building-node.hh"
#include "building-controller.hh"
#include "grand-hall-function.hh"
#include "leveled-building-function.hh"
#include "to-string.hh"
#include "level-manager.hh"
#include "attack-state.hh"
#include "death-event.hh"
#include <cstdlib>

ActorFactory::ActorFactory(std::string d)
{
	directory = d;
	Json* files = Json::parseFile(directory + "config.json");

	dwarfJson = Json::parseFile(directory + files->getMember("dwarfconfig")->getString());
	orcJson = Json::parseFile(directory + files->getMember("orcconfig")->getString());
	terrainJson = Json::parseFile(directory + files->getMember("terrainconfig")->getString());
	grandHallJson = Json::parseFile(directory + files->getMember("grandhallconfig")->getString());
	apothecaryJson = Json::parseFile(directory + files->getMember("apothecaryconfig")->getString());
	blacksmithJson = Json::parseFile(directory + files->getMember("blacksmithconfig")->getString());
	charactersMade = 0;

	characterControllers = new DynamicArray<StateMachine*>();
}

void ActorFactory::createDwarf(int x, int y)
{
	DEBUG_LOG("actorfactory", "Creating dwarf");

	// Create 
	t_rgb rgb;
	char icon = dwarfJson->getMember("icon")->getString()[0];
	AsciiDrawable* dwarfDrawable = new AsciiDrawable(CHARACTER_LEVEL, icon, rgb);
	Character* dwarf = new Character(x, y, CHARACTER_COLLISION, dwarfDrawable, dwarfJson->getMember("health")->getNumber());
	dwarf->isMoveable = true;
	dwarf->dispatcher = new Dispatcher();

	//set their values
	float gold = dwarfJson->getMember("gold")->getNumber();
	float variance = dwarfJson->getMember("goldvariance")->getNumber();
	//set gold randomly in the range of (gold - variance/2) to (gold + variance/2)
	gold += (rand() % (int)variance) - (variance / 2);
	dwarf->attributes->insert("gold", gold);
	dwarf->attributes->insert("movedelay", dwarfJson->getMember("movedelay")->getNumber());
	dwarf->attributes->insert("timesincelastmove", (((double)rand()) / ((double)RAND_MAX)) * dwarf->attributes->get("movedelay"));
	dwarf->attributes->insert("thirstmeter", 100.0f);
	dwarf->attributes->insert("thirstrate", dwarfJson->getMember("thirstrate")->getNumber());
	dwarf->attributes->insert("sleepmeter", 100.0f);
	dwarf->attributes->insert("sleeprate", dwarfJson->getMember("sleeprate")->getNumber());
	dwarf->attributes->insert("attackdelay", dwarfJson->getMember("attackdelay")->getNumber());
	dwarf->attributes->insert("minstatetime", dwarfJson->getMember("minstatetime")->getNumber());
	dwarf->attributes->insert("hitchance", dwarfJson->getMember("hitchance")->getNumber());
	dwarf->attributes->insert("movecost", dwarfJson->getMember("movecost")->getNumber());
	dwarf->attributes->insert("drinkthreshold", dwarfJson->getMember("drinkthreshold")->getNumber());
	dwarf->attributes->insert("sleepthreshold", dwarfJson->getMember("sleepthreshold")->getNumber());
	dwarf->attributes->insert("enteringbuilding", 0.0f);
	dwarf->attributes->insert("exitingbuilding", 0.0f);
	dwarf->attributes->insert("timesincelastattack", 0.0f);
	dwarf->attributes->insert("team", DWARF_TEAM);
	dwarf->attributes->insert("weapon", NO_WEAPON);
	dwarf->attributes->insert("potion", NO_POTION);
	dwarf->type = "dwarf";
	dwarf->id = charactersMade;

	// Create controller
	Trie<IState*>* stateMap = new Trie<IState*>();
	IState* idleState = new BrownianState();
	stateMap->insert("idle", idleState);
	IState* moveState = new MoveState();
	stateMap->insert("move", moveState);
	IState* attackState = new AttackState();
	stateMap->insert("attack", attackState);
	StateMachine* stateMachine = new StateMachine(stateMap, "idle", dwarf);
	dwarf->dispatcher->addListener(STATE_TRANSITION_NAME, stateMachine->getListener());
	dwarf->dispatcher->addListener(DEATH_EVENT_TYPE, LevelManager::getInstance()->getListener());

	// Register the new actor with the rest of the game
	SceneManager::getInstance()->addSceneNode(dwarf);
	SceneManager::getInstance()->addActor(dwarf);
	SceneManager::getInstance()->addTickable(stateMachine);
	characterControllers->push(stateMachine);
	SceneManager::getInstance()->addTickable(dwarf->dispatcher);

	charactersMade++;
}

void ActorFactory::createOrc(int x, int y)
{
	DEBUG_LOG("actorfactory", "Creating orc");

	// Create 
	t_rgb rgb;
	char icon = orcJson->getMember("icon")->getString()[0];
	AsciiDrawable* orcDrawable = new AsciiDrawable(CHARACTER_LEVEL, icon, rgb);
	Character* orc = new Character(x, y, CHARACTER_COLLISION, orcDrawable, orcJson->getMember("health")->getNumber());
	orc->isMoveable = false;
	orc->dispatcher = new Dispatcher();
	orc->type = "orc";
	orc->id = charactersMade;

	//set their values
	float gold = orcJson->getMember("gold")->getNumber();
	float variance = orcJson->getMember("goldvariance")->getNumber();
	//set gold randomly in the range of (gold - variance/2) to (gold + variance/2)
	gold += (rand() % (int)variance) - (variance / 2);
	orc->attributes->insert("gold", gold);
	orc->attributes->insert("movedelay", orcJson->getMember("movedelay")->getNumber());
	orc->attributes->insert("timesincelastmove", (((double)rand()) / ((double)RAND_MAX)) * orc->attributes->get("movedelay"));
	orc->attributes->insert("attackdelay", orcJson->getMember("attackdelay")->getNumber());
	orc->attributes->insert("minstatetime", orcJson->getMember("minstatetime")->getNumber());
	orc->attributes->insert("hitchance", orcJson->getMember("hitchance")->getNumber());
	orc->attributes->insert("enteringbuilding", 0.0f);
	orc->attributes->insert("timesincelastattack", 0.0f);
	orc->attributes->insert("team", ORC_TEAM);
	orc->attributes->insert("weapon", NO_WEAPON);
	orc->attributes->insert("potion", NO_POTION);

	// Create controller
	Trie<IState*>* stateMap = new Trie<IState*>();
	IState* idleState = new BrownianState();
	stateMap->insert("idle", idleState);
	IState* attackState = new AttackState();
	stateMap->insert("attack", attackState);	
	StateMachine* stateMachine = new StateMachine(stateMap, "idle", orc);
	orc->dispatcher->addListener(STATE_TRANSITION_NAME, stateMachine->getListener());
	orc->dispatcher->addListener(DEATH_EVENT_TYPE, LevelManager::getInstance()->getListener());

	// Register the new actor with the rest of the game
	SceneManager::getInstance()->addSceneNode(orc);
	SceneManager::getInstance()->addActor(orc);
	SceneManager::getInstance()->addTickable(stateMachine);
	characterControllers->push(stateMachine);
	SceneManager::getInstance()->addTickable(orc->dispatcher);
	charactersMade++;
}

void ActorFactory::createTree(int x, int y)
{
	DEBUG_LOG("actorfactory", "Creating tree");

	// Create tree
	t_rgb rgb;
	char icon = terrainJson->getMember("tree")->getString()[0];
	AsciiDrawable* treeDrawable = new AsciiDrawable(TERRAIN_LEVEL, icon, rgb);
	Terrain* tree = new Terrain(x, y, TERRAIN_COLLISION, treeDrawable, "tree");

	// Register the new actor with the rest of the game
	SceneManager::getInstance()->addSceneNode(tree);
	SceneManager::getInstance()->addActor(tree);
}

void ActorFactory::createMountain(int x, int y)
{
	DEBUG_LOG("actorfactory", "Creating tree");

	// Create tree
	t_rgb rgb;
	char icon = terrainJson->getMember("mountain")->getString()[0];
	AsciiDrawable* mountainDrawable = new AsciiDrawable(TERRAIN_LEVEL, icon, rgb);
	Terrain* mountain = new Terrain(x, y, TERRAIN_COLLISION, mountainDrawable, "mountain");

	// Register the new actor with the rest of the game
	SceneManager::getInstance()->addSceneNode(mountain);
	SceneManager::getInstance()->addActor(mountain);
}

void ActorFactory::createGrandHall(int x, int y)
{
	// Create 
	t_rgb rgb;
	char icon = grandHallJson->getMember("icon")->getString()[0];
	AsciiDrawable* grandHallDrawable = new AsciiDrawable(BUILDING_LEVEL, icon, rgb);
	int size = grandHallJson->getMember("size")->getNumber();
	Building* grandHall = new Building(x, y, size, BUILDING_COLLISON, grandHallDrawable);
	grandHall->type = "grandhall";
	grandHall->entranceX = x;
	grandHall->entranceY = y - 1;
	grandHall->attributes->insert("upgradecost", grandHallJson->getMember("upgradecost")->getNumber());
	grandHall->attributes->insert("recruitcost", grandHallJson->getMember("recruitcost")->getNumber());
	grandHall->attributes->insert("apothecarycost", grandHallJson->getMember("apothecarycost")->getNumber());
	grandHall->attributes->insert("blacksmithcost", grandHallJson->getMember("blacksmithcost")->getNumber());
	grandHall->attributes->insert("apothecariesbuilt", 0.0f);
	grandHall->attributes->insert("blacksmithsbuilt", 0.0f);

	buildWalls(grandHall);

	// Add functionality
	grandHall->function = new GrandHallFunction(grandHall, directory);

	// Register with the world
	SceneManager::getInstance()->addTickable(new BuildingController(grandHall));
	SceneManager::getInstance()->addSceneNode(grandHall);
	SceneManager::getInstance()->addActor(grandHall);

}

void ActorFactory::createApothecary(int x, int y)
{
	// Create 
	t_rgb rgb;
	char icon = apothecaryJson->getMember("icon")->getString()[0];
	AsciiDrawable* apothecaryDrawable = new AsciiDrawable(BUILDING_LEVEL, icon, rgb);
	int size = apothecaryJson->getMember("size")->getNumber();
	Building* apothecary = new Building(x, y, size, BUILDING_COLLISON, apothecaryDrawable);
	apothecary->type = "apothecary";
	apothecary->entranceX = x;
	apothecary->entranceY = y - 1;
	apothecary->attributes->insert("upgradecost", apothecaryJson->getMember("upgradecost")->getNumber());

	buildWalls(apothecary);

	// Add functionality
	apothecary->function = new LeveledBuildingFunction(apothecary);

	// Register with the world
	SceneManager::getInstance()->addTickable(new BuildingController(apothecary));
	SceneManager::getInstance()->addSceneNode(apothecary);
	SceneManager::getInstance()->addActor(apothecary);

}

void ActorFactory::createBlacksmith(int x, int y)
{
	// Create 
	t_rgb rgb;
	char icon = blacksmithJson->getMember("icon")->getString()[0];
	AsciiDrawable* blacksmithDrawable = new AsciiDrawable(BUILDING_LEVEL, icon, rgb);
	int size = blacksmithJson->getMember("size")->getNumber();
	Building* blacksmith = new Building(x, y, size, BUILDING_COLLISON, blacksmithDrawable);
	blacksmith->type = "blacksmith";
	blacksmith->entranceX = x;
	blacksmith->entranceY = y - 1;
	blacksmith->attributes->insert("upgradecost", blacksmithJson->getMember("upgradecost")->getNumber());

	buildWalls(blacksmith);

	// Add functionality
	blacksmith->function = new LeveledBuildingFunction(blacksmith);

	// Register with the world
	SceneManager::getInstance()->addTickable(new BuildingController(blacksmith));
	SceneManager::getInstance()->addSceneNode(blacksmith);
	SceneManager::getInstance()->addActor(blacksmith);

}

// Builds walls for a building
void ActorFactory::buildWalls(Building* b)
{
	int x = b->getX();
	int y = b->getY();
	int size = b->size;
	AsciiDrawable* drawable = (AsciiDrawable*)b->drawable;

	// top wall
	for (int xpos = x + 1, i = 0; i <= size; i++, xpos++)
	{
		BuildingNode* node = new BuildingNode(xpos, y, BUILDING_COLLISON,  drawable, b);
		SceneManager::getInstance()->addSceneNode(node);
		SceneManager::getInstance()->addActor(node);
	}

	// bottom wall
	for (int xpos = x, i = 0; i <= size + 1; i++, xpos++)
	{
		BuildingNode* node = new BuildingNode(xpos, y + size + 1, BUILDING_COLLISON,  drawable, b);
		SceneManager::getInstance()->addSceneNode(node);
		SceneManager::getInstance()->addActor(node);
	}

	// left wall
	for (int ypos = y + 1, i = 0; i <= size; i++, ypos++)
	{
		BuildingNode* node = new BuildingNode(x, ypos, BUILDING_COLLISON,  drawable, b);
		SceneManager::getInstance()->addSceneNode(node);
		SceneManager::getInstance()->addActor(node);
	}

	// right wall
	for (int ypos = y, i = 0; i <= size + 1; i++, ypos++)
	{
		BuildingNode* node = new BuildingNode(x + size + 1, ypos, BUILDING_COLLISON, drawable, b);
		SceneManager::getInstance()->addSceneNode(node);
		SceneManager::getInstance()->addActor(node);
	}
}

void ActorFactory::deleteCharacter(Character* character)
{
	SceneManager::getInstance()->getSceneGraph()->removeSceneNode(character);
	SceneManager::getInstance()->getActors()->removeElement(character);
	SceneManager::getInstance()->getTickables()->removeElement(characterControllers->get(character->id));
	SceneManager::getInstance()->getTickables()->removeElement(character->dispatcher);
}