#ifndef _I_EVENT_HH_
#define _I_EVENT_HH_

#include <string>

// Event interface that requires children to have a type
class IEvent
{
public:
	virtual ~IEvent(){}
	virtual std::string getType() = 0;
};

#endif