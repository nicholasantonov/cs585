#ifndef _CURSOR_STATE_HH_
#define _CURSOR_STATE_HH_

#include "i-state.hh"

class CursorState : public IState
{
public:
	CursorState();

	void tick(float dt, IActor* actor);
	void setUp(IActor* actor);
	void breakDown(IActor* actor);

	class OnKey : public IListener
	{
	public:
		IState* instance;
		void handle(IEvent* e);
	} _onKey;

	IActor** levelSelectedActor;
};

#endif