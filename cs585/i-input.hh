#ifndef _I_INPUT_HH_
#define _I_INPUT_HH_

#include "i-tickable.hh"
#include "dispatcher.hh"

class IInput
{
public:
	virtual ~IInput(){}
	virtual void tick(float dt, Dispatcher* d) = 0;
};

#endif