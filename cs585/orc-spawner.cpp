#include "orc-spawner.hh"

OrcSpawner::OrcSpawner(int x, int y, float delay, ActorFactory* f) :
timeSinceLastSpawn(0),
spawnDelay(delay),
xpos(x),
ypos(y),
factory(f)
{}

void OrcSpawner::tick(float dt)
{
	timeSinceLastSpawn += dt;
	if (timeSinceLastSpawn > spawnDelay)
	{
		timeSinceLastSpawn -= spawnDelay;
		factory->createOrc(xpos, ypos);
	}

}