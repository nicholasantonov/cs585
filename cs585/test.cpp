#include "test.hh"

#include <cstdlib>
#include <iostream>
#include <string>

// Default Constructor creates a new test category with name "Unit"
Test::Test()
{
	quitOnFail = false;
	testName = "Unit";
	testIndex = -1;
	descriptions = new DynamicArray<std::string>();
	results = new DynamicArray<bool>();
}

// Constructor that makes a new test category with the given name
Test::Test(std::string name)
{
	quitOnFail = false;
	testName = name;
	testIndex = -1;
	descriptions = new DynamicArray<std::string>();
	results = new DynamicArray<bool>();
}

// Constructor that makes a new test category with the given name and sets the desired fail behaviour
Test::Test(std::string name, bool failSetting)
{
	quitOnFail = failSetting;
	testName = name;
	testIndex = -1;
	descriptions = new DynamicArray<std::string>();
	results = new DynamicArray<bool>();
}

// Creates a new test that will be associated with any conditions given later
void Test::define(std::string description)
{
	testIndex++;
	descriptions->push(description);
	results->push(true);
}

// Sets if the test suite should quit after failing a test
void Test::exitOnFail(bool failSetting)
{
	quitOnFail = failSetting;
}

// This function fails the currently defined test if it is passed a false
void Test::expect(bool result)
{
	if ((testIndex >= 0) && !result)
	{
		results->set(result, (unsigned int)testIndex);

		if (quitOnFail)
		{
			std::cout << "Test Failed; aborting early" << std::endl;
			run();
			// I have to use assert since we cannot use stdlib to use "abort()"
			abort();
		}
	}
}

// Formats and prints out all test results
void Test::run()
{
	if (testIndex >= 0)
	{
		unsigned int testsPassed = 0;
		unsigned int numberOfTests = (unsigned int)(testIndex + 1);

		std::cout << "Loaded " << testName << " Test (" << numberOfTests << " tests found)" << std::endl;
		for (unsigned int i = 0; i < numberOfTests; i++)
		{
			char* status = (char*)(results->get(i) ? "[PASS]" : "[FAIL]");
			if (results->get(i))
			{
				testsPassed++;
			}
			std::cout << status << " " << descriptions->get(i) << std::endl;
		}
		printTestCompletion(testsPassed);
	}
}

// Prints how many tests have completed out of the total number of tests
void Test::printTestCompletion(unsigned int testsPassed)
{
	std::cout << testsPassed << "/" << (testIndex + 1) << " Passed" << std::endl;
}