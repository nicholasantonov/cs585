#ifndef _ATTACK_STATE_HH_
#define _ATTACK_STATE_HH_

#include "i-state.hh"
#include "character.hh"

class AttackState : public IState
{
public:
	AttackState();

	void tick(float dt, IActor* actor);
	void setUp(IActor* actor);
	void breakDown(IActor* actor);
private:
	void attackWith(Character* actor);
};

#endif