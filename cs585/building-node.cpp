#include "building-node.hh"

BuildingNode::BuildingNode(int xPos, int yPos, unsigned long collisionMap, AsciiDrawable* d, Building* b) :
	building(b)
{
	drawable = d;
	x = xPos;
	y = yPos;
	map = collisionMap;
	attributes = b->attributes;
}

std::string BuildingNode::getDescription()
{
	return building->getDescription();
}

void BuildingNode::dealDamage(float d)
{
	building->dealDamage(d);
}

bool BuildingNode::canBeInteractedWith()
{
	return building->canBeInteractedWith();
}

void BuildingNode::handleInput(int input, int inputX, int inputY)
{
	building->handleInput(input, inputX, inputY);
}

std::string BuildingNode::getType()
{
	return building->getType();
}