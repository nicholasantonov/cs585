#include "dispatcher.hh"
#include "debug.hh"
#include "queue.hh"

// Default contstructor 
Dispatcher::Dispatcher()
{
	DEBUG_LOG("dispatcher", "Creating new dispatcher");
	listeners = new Trie<DynamicArray<IListener*>*>();
	events = new Queue<IEvent*>();
	listenersToAdd = new Queue<IListener*>();
	channelsToAddTo = new Queue<std::string>();
	listenersToRemove = new Queue<IListener*>();
	channelsToRemoveFrom = new Queue<std::string>();
}

// Destructor
Dispatcher::~Dispatcher()
{
	DEBUG_LOG("dispatcher", "Deleting dispatcher");
}

// Add a listener to the dispatcher that should be called when the given eventType occurs
void Dispatcher::addQueuedListener(std::string eventType, IListener* listener)
{
	DEBUG_LOG("dispatcher", "Adding listener to " + eventType);
	if (!listeners->wasSet(eventType))
	{
		listeners->insert(eventType, new DynamicArray<IListener*>);
	}
	listeners->get(eventType)->push(listener);
}

// Removes a listener from the Dispatcher
void Dispatcher::removeQueuedListener(std::string eventType, IListener* listener)
{
	DEBUG_LOG("dispatcher", "Removing listener from " + eventType);
	if (listeners->wasSet(eventType))
	{
		DynamicArray<IListener*>* arr = listeners->get(eventType);
		for (unsigned int i = 0; i < arr->length(); i++)
		{
			if (arr->get(i) == listener)
			{
				arr->remove(i);
				return;
			}
		}
	}
	DEBUG_WARN("dispatcher", "No listener found to remove from " + eventType);
}

void Dispatcher::dispatch(IEvent* ev)
{
	DEBUG_LOG("dispatcher", "pushing event to be sent on tick");
	events->push(ev);
}

// Empties the event queue
void Dispatcher::tick(float){
	while (!events->isEmpty())
	{
		DEBUG_LOG("dispatcher", "popping and sending event");
		dispatchEvent(events->pop());
	}
	addQueuedListeners();
	removeQueuedListeners();
}

// Calls handle on all listeners asoociated with given event type
void Dispatcher::dispatchEvent(IEvent* ev)
{	
	DEBUG_LOG("dispatcher", "dispatching event " + ev->getType());
	if (listeners->wasSet(ev->getType()))
	{
		DynamicArray<IListener*>* arr = listeners->get(ev->getType());
		for (unsigned int i = 0; i < arr->length(); i++)
		{
			arr->get(i)->handle(ev);
		}
	}
	delete ev;
}

void Dispatcher::addQueuedListeners()
{
	while (!listenersToAdd->isEmpty())
	{
		addQueuedListener(channelsToAddTo->pop(), listenersToAdd->pop());
	}
}

void Dispatcher::removeQueuedListeners()
{
	while (!listenersToRemove->isEmpty())
	{
		removeQueuedListener(channelsToRemoveFrom->pop(), listenersToRemove->pop());
	}
}

void Dispatcher::addListener(std::string eventType, IListener* listener)
{
	DEBUG_LOG("dispatcher", "adding listener to " + eventType + " queue");
	channelsToAddTo->push(eventType);
	listenersToAdd->push(listener);
}

void Dispatcher::removeListener(std::string eventType, IListener* listener)
{
	DEBUG_LOG("dispatcher", "removing listener from " + eventType + " queue");
	channelsToRemoveFrom->push(eventType);
	listenersToRemove->push(listener);

}