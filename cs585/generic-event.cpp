#include "generic-event.hh"

// Default constructor with an empty string as the type
GenericEvent::GenericEvent()
{
	type = "";
}

// Default constructor that sets the type to the passed string
GenericEvent::GenericEvent(std::string t)
{
	type = t;
}

// Returns the type of the event
std::string GenericEvent::getType()
{
	return type;
}