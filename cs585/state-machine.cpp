#include "state-machine.hh"

#include "state-transition.hh"

StateMachine::StateMachine(
	Trie<IState*>* lookUp, 
	std::string state,
	IActor* a) :
		stateLookUp(lookUp),
		currentState(stateLookUp->get(state)),
		actor(a)
{
	DEBUG_LOG("statemachine", "Constructing State Machine");
	_onStateTransition.setInstance(this);
	// set initial state to notify statemachine on transition
	currentState->subscribe(STATE_TRANSITION_NAME, &_onStateTransition);
	currentState->setUp(actor);
}

StateMachine::~StateMachine()
{
	DEBUG_LOG("statemachine", "Destructing State Machine");
	delete stateLookUp;
}

void StateMachine::tick(float dt)
{
	currentState->preTick(dt, actor);
}

// Constructor for the event handler;
StateMachine::OnStateTransition::OnStateTransition(){}

void StateMachine::OnStateTransition::setInstance(StateMachine* i)
{
	DEBUG_LOG("statemachine", "Setting instance of machine in the listener");
	instance = i;
}

void StateMachine::OnStateTransition::handle(IEvent* ev)
{
	DEBUG_LOG("statemachine", "Switching States");
	TransitionEvent* e = (TransitionEvent*)ev;
	instance->currentState->unSubscribe(STATE_TRANSITION_NAME, &(instance->_onStateTransition));
	instance->currentState->breakDown(instance->actor);
	instance->currentState = instance->stateLookUp->get(e->state);
	instance->currentState->subscribe(STATE_TRANSITION_NAME, &(instance->_onStateTransition));
	instance->currentState->setUp(instance->actor);
}

IListener* StateMachine::getListener()
{
	return &_onStateTransition;
}