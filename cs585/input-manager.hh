#ifndef _INPUT_MANAGER_HH_
#define _INPUT_MANAGER_HH_

#include "dispatcher.hh"
#include "i-input.hh"

class InputManager
{
public:
	static InputManager* getInstance();

	void addListener(std::string eventType, IListener* listener);
	void setInputHandler(IInput* handler);
	void removeListener(std::string eventType, IListener* listener);
	void dispatch(IEvent* ev);
	void tick(float dt);

private:
	InputManager();
	~InputManager();

	InputManager(InputManager const&);
	void operator=(InputManager const&);

	Dispatcher* dispatcher;
	IInput* inputHandler;
	

	static InputManager* instance;
};

#endif