#ifndef _GAME__HH_
#define _GAME__HH_

#define TIME_TO_RUN 30

class Game
{
public:
	Game();

	void run();
};

#endif