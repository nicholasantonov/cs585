#ifndef _BROWNIAN_STATE_HH_
#define _BROWNIAN_STATE_HH_

#include "i-state.hh"

class BrownianState : public IState
{
public:
	BrownianState();

	void tick(float dt, IActor* actor);
	void setUp(IActor* actor);
	void breakDown(IActor* actor);
};

#endif