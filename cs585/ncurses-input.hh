#ifndef _NCURSES_INPUT_HH_
#define _NCURSES_INPUT_HH_

#include "i-input.hh"

class NcursesInput : public IInput
{
public:
	NcursesInput();
	void tick(float dt, Dispatcher* d);
};

#endif