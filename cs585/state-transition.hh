#ifndef _STATE_TRANSITION_HH_
#define _STATE_TRANSITION_HH_

#include "i-event.hh"
#include "dynamic-array.hh"
#include <string>

class TransitionEvent : public IEvent
{
public:
	TransitionEvent();
	TransitionEvent(std::string);
	virtual ~TransitionEvent();
	virtual std::string getType();
	std::string state;
};

#endif