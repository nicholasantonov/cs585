#ifndef _GRAND_HALL_FUNCTION_HH_
#define _GRAND_HALL_FUNCTION_HH_

#include "i-function.hh"
#include "building.hh"
#include "actor-factory.hh"
#include <string>

class GrandHallFunction : public IFunction
{
public:
	GrandHallFunction(Building* b, std::string d);
	void handleInput(int input, int inputX, int inputY);
private:
	std::string directory;
	Building* building;
	ActorFactory factory;
};
#endif