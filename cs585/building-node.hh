#ifndef _BUILDING_NODE_HH_
#define _BUILDING_NODE_HH_

#include "i-actor.hh"
#include "building.hh"
#include "ascii-drawable.hh"

class BuildingNode : public IActor
{
public:
	BuildingNode(int x, int y, unsigned long collision, AsciiDrawable* a, Building* b);

	std::string getDescription();
	void dealDamage(float);
	bool canBeInteractedWith();
	void handleInput(int input, int inputX, int inputY);
	std::string getType();

	std::string type;

private:
	Building* building;
};

#endif