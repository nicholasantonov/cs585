#ifndef _MOVE_STATE_HH_
#define _MOVE_STATE_HH_

#include "i-state.hh"

class  MoveState : public IState
{
public:
	 MoveState();

	void tick(float dt, IActor* actor);
	void setUp(IActor* actor);
	void breakDown(IActor* actor);
};

#endif