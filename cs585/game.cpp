#include "game.hh"

#define STARTING_LEVEL_NUMBER 0

#include "debug.hh"
#include "level-manager.hh"
#include "json.hh"
#include <ctime>
#include <iomanip>
#include <cstdlib>

Game::Game()
{
	Json* levelsJson = Json::parseFile("json/levels.json");
	std::string levelPath = levelsJson->get(STARTING_LEVEL_NUMBER)->getString();

	LevelManager::getInstance()->loadLevel("json/dwarves1/");
}

void Game::run()
{
	srand((unsigned int)(time(NULL)));
	DEBUG_INITIALIZE();
	DEBUG_LOG("mainloop", "Initializing");
	clock_t lastTime = clock();

	float dt;

	while (1)
	{
		dt = (float)((clock() - lastTime) / (CLOCKS_PER_SEC * 1.0));
		lastTime = clock();
		DEBUG_LOG("mainloop", "looping");

		LevelManager::getInstance()->tick(dt);
	}
	DEBUG_LOG("gameplay", "exiting");
}