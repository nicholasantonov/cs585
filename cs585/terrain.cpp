#include "terrain.hh"

Terrain::Terrain()
{
	x = 0;
	y = 0;
	map = 0;
	attributes = new Trie<float>;
	type = "generic terrain";
}

Terrain::Terrain(int xPos, int yPos, unsigned long collisionMap, IDrawable* d, std::string t)
{
	drawable = d;
	x = xPos;
	y = yPos;
	map = collisionMap;
	attributes = new Trie<float>;
	type = t;
}

// This terrain cannot take damage
void Terrain::dealDamage(float){}

// This terrain does not need to be ticked
void Terrain::tick(float){}

std::string Terrain::getDescription()
{
	std::string ret = "A " + type;
	return ret;
}

bool Terrain::canBeInteractedWith()
{
	return false;
}

void Terrain::handleInput(int input, int inputX, int inputY) {}

std::string Terrain::getType()
{
	return type;
}