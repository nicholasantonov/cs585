#include "level-manager.hh"

#include "scene-manager.hh"
#include "i-level.hh"
#include "gnomelevel.hh"
#include "i-listener.hh"

LevelManager* LevelManager::instance = nullptr;

LevelManager* LevelManager::getInstance()
{
	if (instance == nullptr)
	{
		instance = new LevelManager();
	}
	return instance;
}

LevelManager::LevelManager()
{
	level = nullptr;
}

void LevelManager::setLevel(ILevel* l)
{
	if (level != nullptr)
	{
		delete level;
	}
	level = l;
}

void LevelManager::loadLevel(std::string directory)
{
	DEBUG_LOG("level", "loading level at " + directory);

	setLevel(new GnomeLevel());
	level->loadLevel(directory);
}

void LevelManager::tick(float dt)
{
	level->tick(dt);
}

int LevelManager::getGold()
{
	return level->getGold();
}

void LevelManager::addGold(int g)
{
	level->addGold(g);
}

IListener* LevelManager::getListener()
{
	return level->getListener();
}