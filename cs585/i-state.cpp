#include "i-state.hh"

#include "state-transition.hh"

IState::IState()
{
	DEBUG_LOG("state", "Constructing state");
	machines = new Dispatcher();
}

IState::~IState()
{
	DEBUG_LOG("state", "Destructing state");
	delete machines;
}

void IState::preTick(float dt, IActor* actor)
{
	machines->tick(dt);
	tick(dt, actor);
}

void IState::subscribe(std::string eventType, IListener* listener)
{
	DEBUG_LOG("state", "Adding statemachine subscription");
	machines->addListener(eventType, listener);
}

void IState::unSubscribe(std::string eventType, IListener* listener)
{
	DEBUG_LOG("state", "Removing statemachine subscription");
	machines->removeListener(eventType, listener);
}

std::string IState::getType()
{
	return type;
}

void IState::transition(std::string newState)
{
	TransitionEvent* ev = new TransitionEvent();
	ev->state = newState;
	machines->dispatch(ev);
}