#ifndef _DISPATCHER_HH_
#define _DISPATCHER_HH_

#include "i-event.hh"
#include "i-listener.hh"
#include "dynamic-array.hh"
#include "queue.hh"
#include "i-tickable.hh"
#include <string>


// Dispatcher class to keep track of listeners and propogate events
class Dispatcher : public ITickable
{
public:
	Dispatcher();
	virtual ~Dispatcher();
	void addListener(std::string eventType, IListener* listener);
	void removeListener(std::string eventType, IListener* listener);
	void dispatch(IEvent* ev);
	virtual void tick(float dt);
private:
	Trie<DynamicArray<IListener*>*>* listeners;
	Queue<IEvent*>* events;
	Queue<IListener*>* listenersToAdd;
	Queue<std::string>* channelsToAddTo;
	Queue<IListener*>* listenersToRemove;
	Queue<std::string>* channelsToRemoveFrom;


	void dispatchEvent(IEvent* ev);
	void addQueuedListeners();
	void addQueuedListener(std::string eventType, IListener* listener);
	void removeQueuedListeners();
	void removeQueuedListener(std::string eventType, IListener* listener);
};

#endif