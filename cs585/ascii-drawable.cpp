#include "ascii-drawable.hh"

AsciiDrawable::AsciiDrawable()
{
	zIndex = 0;
	icon = '.';
	colors.red = 0;
	colors.green = 0;
	colors.blue = 0;
}

AsciiDrawable::AsciiDrawable(int newZ, char newIcon, t_rgb newColors)
{
	zIndex = newZ;
	icon = newIcon;
	colors = newColors;
}