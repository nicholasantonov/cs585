#ifndef _INPUT_EVENT_HH_
#define _INPUT_EVENT_HH_

#include "i-event.hh"
#include <ncurses.h>

class InputEvent : public IEvent
{
public:
	InputEvent(int key);
	std::string getType();
	int input;

	static int UP;
	static int DOWN;
	static int LEFT;
	static int RIGHT;
};

#endif