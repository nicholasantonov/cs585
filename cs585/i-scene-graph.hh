#ifndef _SCENE_GRAPH_HH_
#define _SCENE_GRAPH_HH_

#include "scene-node.hh"
#include "dynamic-array.hh"

typedef struct t_coordSet
{
	int minX;
	int minY;
	int maxX;
	int maxY;
} t_coordSet;

class ISceneGraph
{
public:
	virtual ~ISceneGraph(){}
	virtual void addSceneNode(SceneNode* node) = 0;
	virtual void removeSceneNode(SceneNode* node) = 0;
	virtual void updateSceneNode(SceneNode* node, int x, int y) = 0;
	virtual bool canMoveTo(SceneNode* node, int x, int y) = 0;
	virtual DynamicArray<SceneNode*>* getColliders(SceneNode* node) = 0;
	virtual DynamicArray<SceneNode*>* getColliders(int x, int y) = 0;
	virtual DynamicArray<SceneNode*>* getColliders(int x, int y, int radius) = 0;
	virtual DynamicArray<SceneNode*>* getColliders(int cornerX1, int cornerY1, int cornerX2, int cornerY2) = 0;
	virtual t_coordSet getBounds() = 0;
	virtual SceneNode* getTopDrawableNodeAt(int x, int y) = 0;
	virtual void setDefaultTile(SceneNode* node) = 0;
};

#endif