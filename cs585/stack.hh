#ifndef _STACK_HH_
#define _STACK_HH_

#include "dynamic-array.hh"
#include "debug.hh"

template <class Elem>
class Stack
{
public:
	// Default Constructor
	Stack()
	{
		DEBUG_LOG("stack", "default constructing");
		data = DynamicArray<Elem>();
	}

	// Constructor where user can specify an initial capacity
	Stack(unsigned int initialCap)
	{
		DEBUG_LOG("stack", "constructing with set capacity");
		data = DynamicArray<Elem>(initialCap);
	}

	// Destructor
	~Stack()
	{
		DEBUG_LOG("stack", "deleting");
		delete data;
	}

	// Returns the length of the stack
	unsigned int length()
	{
		DEBUG_LOG("stack", "getting length");
		return data.length();
	}

	// Returns the current number of allocated memory locations for the stack
	unsigned int allocated()
	{
		DEBUG_LOG("stack", "getting allocated space");
		return data.allocated();
	}

	// Adds a new element at the top of the stack
	void push(Elem element)
	{
		DEBUG_LOG("stack", "pushing");
		data.push(element);
	}

	// remove and return the top element of the stack
	Elem pop()
	{
		DEBUG_LOG("stack", "popping");
		return data.remove(data.length() - 1);
	}

	// returns the top element of the stack
	Elem peek()
	{
		DEBUG_LOG("stack", "peeking");
		return data.get(data.length() - 1);
	}

	// Increases the number of allocated positions by the number given
	void allocate(unsigned int newSpaces)
	{
		DEBUG_LOG("stack", "allocating extra space");
		data.allocate(newSpaces);
	}

	// Returns true if the stack is empty
	bool isEmpty()
	{
		DEBUG_LOG("stack", "checking emptiness");
		return (data.length() == 0);
	}

private:
	DynamicArray<Elem> data;
};

#endif