#ifndef _I_FUNCTION_HH_
#define _I_FUNCTION_HH_

class IFunction
{
public:
	virtual void handleInput(int input, int inputX, int inputY) = 0;
};

#endif