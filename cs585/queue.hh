#ifndef _QUEUE_HH_
#define _QUEUE_HH_

#include <utility>
#include "dynamic-array.hh"
#include "debug.hh"

template <class Elem>
class Queue
{
public:
	// Default Constructor
	Queue()
	{
		DEBUG_LOG("queue", "default constructing");
		numberOfElements = 0;
		capacity = 16;
		storage = new Elem[capacity];
		front = 0;
		back = 0;
	}

	// Constructor where user can specify an initial capacity
	Queue(unsigned int initialCap)
	{
		DEBUG_LOG("queue", "constructing with a set initial cap");
		numberOfElements = 0;
		capacity = initialCap;
		storage = new Elem[capacity];
		front = 0;
		back = 0;
	}

	// Destructor
	~Queue()
	{
		DEBUG_LOG("queue", "deconstructing");
		delete [] storage;
	} 

	// Returns the length of the queue
	unsigned int length()
	{
		DEBUG_LOG("queue", "getting length");
		return numberOfElements;
	}

	// Returns the current number of allocated memory locations for the queue
	unsigned int allocated()
	{
		DEBUG_LOG("queue", "getting allocated space");
		return capacity;
	}

	// Adds a new element at the front of the queue
	void push(Elem element)
	{
		DEBUG_LOG("queue", "pushing new element");
		if (numberOfElements == capacity)
		{
			DEBUG_WARN("queue", "push caused an expansion");
			allocate(calculateCapacityNeeded());
		}
		numberOfElements++;
		storage[front] = element;
		// Move front forward, or cycle to the beginning of the array if the end is reached
		front = (front + 1) % capacity;
	}

	// returns the element at the back of the queue and removes it
	Elem pop()
	{
		DEBUG_LOG("queue", "popping an element");
		unsigned int oldBack = back;
		numberOfElements--;
		// Move back forward, or cycle to the beginning of the array if the end is reached
		back = (back + 1) % capacity;
		return storage[oldBack];
	}

	// returns the element at the back of the queue
	Elem peek()
	{
		DEBUG_LOG("queue", "peeking at back element");
		return storage[back];
	}

	// Increases the number of allocated positions by the number given
	void allocate(unsigned int newSpaces)
	{
		DEBUG_LOG("queue", "allocating more space");
		Elem* newStorage = new Elem[capacity + newSpaces];
		for (unsigned int i = 0; i < numberOfElements; i++, back = (back + 1) % capacity)
		{
			newStorage[i] = std::move(storage[back]);
		}

		back = 0;
		front = numberOfElements - 1;
		delete [] storage;

		storage = newStorage;
		capacity += newSpaces;
	}

	// Returns true if the queue is empty
	bool isEmpty()
	{
		DEBUG_LOG("queue", "checking emptiness");
		return (numberOfElements == 0);
	}

protected:
	Elem* storage;
	unsigned int numberOfElements;
	unsigned int capacity;
	unsigned int front;
	unsigned int back;

private:
	// Return what size the dynamic array should grow to
	unsigned int calculateCapacityNeeded()
	{
		return (capacity < 2) ? 2 : (unsigned int)(capacity / 2);
	}
};

#endif