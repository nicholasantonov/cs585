#include "to-string.hh"

#include <sstream>

// To string method since g++ has a bug where to_string is missing
std::string toString(float f)
{
	std::ostringstream buffer;
	buffer << f;
	return buffer.str();
}