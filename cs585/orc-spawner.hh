#ifndef _ORC_SPAWNER_HH_
#define _ORC_SPAWNER_HH_

#include "i-tickable.hh"
#include "actor-factory.hh"

class OrcSpawner : public ITickable
{
public:
	OrcSpawner(int x, int y, float delay, ActorFactory* f);
	void tick(float dt);

private:
	float timeSinceLastSpawn;
	const float spawnDelay;
	const int xpos;
	const int ypos;
	ActorFactory* factory;
};

#endif