#include "move-state.hh"

#include "scene-manager.hh"
#include "i-actor.hh"
#include "character.hh"

MoveState::MoveState()
{

}

// Tick makes actor move to its location, ignores collision and pathfinding as per grapsas
void MoveState::tick(float dt, IActor* actor)
{
	((Character*)actor)->simulateNeeds(dt);
	actor->attributes->insert("timesincelastmove", (actor->attributes->get("timesincelastmove") + dt));

	if (actor->attributes->get("timesincelastmove") > actor->attributes->get("movedelay"))
	{
		actor->attributes->insert("timesincelastmove", (actor->attributes->get("timesincelastmove") - actor->attributes->get("movedelay")));

		int x = actor->getX();
		int y = actor->getY();
		int targetX = actor->attributes->get("targetx");
		int targetY = actor->attributes->get("targety");

		if (x == targetX && y == targetY)
		{
			transition("idle");
			return;
		}

		// Calculate where to move to
		if (targetX < x)
		{
			x -= 1;
		}
		else if (targetX > x)
		{
			x += 1;
		}

		if (targetY < y)
		{
			y -= 1;
		}
		else if (targetY > y)
		{
			y += 1;
		}

		SceneManager::getInstance()->getSceneGraph()->updateSceneNode(actor, x, y);

		// Check to see if attack state needs to be triggereds
	}
}

void MoveState::setUp(IActor* actor)
{

}

void MoveState::breakDown(IActor* actor)
{

}