#ifndef _INTERACT_STATE_HH_
#define _INTERACT_STATE_HH_

#include "i-state.hh"

class InteractState : public IState
{
public:
	InteractState(Trie<DynamicArray<std::string>*>*);

	void tick(float dt, IActor* actor);
	void setUp(IActor* actor);
	void breakDown(IActor* actor);

	class OnKey : public IListener
	{
	public:
		IState* instance;
		void handle(IEvent* e);
	} _onKey;

	IActor** levelSelectedActor;

private:
	Trie<DynamicArray<std::string>*>* menus;
};

#endif