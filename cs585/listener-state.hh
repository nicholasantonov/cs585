#ifndef _Listener_STATE_HH_
#define _Listener_STATE_HH_

#include "i-state.hh"
#include "i-listener.hh"

class ListenerState : public IState
{
public:
	ListenerState(IListener*);

	void tick(float dt, IActor* actor);
	void setUp(IActor* actor);
	void breakDown(IActor* actor);

protected:
	IListener* listener;
};

#endif