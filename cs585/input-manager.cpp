#include "input-manager.hh"

InputManager* InputManager::instance = nullptr;

InputManager* InputManager::getInstance()
{
	if (instance == nullptr)
	{
		instance = new InputManager();
	}
	return instance;
}

InputManager::~InputManager()
{
	delete dispatcher;
	delete inputHandler;
}

void InputManager::addListener(std::string eventType, IListener* listener)
{
	dispatcher->addListener(eventType, listener);
}

void InputManager::setInputHandler(IInput* handler)
{
	if (inputHandler != nullptr)
	{
		delete inputHandler;
	}
	inputHandler = handler;
}

void InputManager::removeListener(std::string eventType, IListener* listener)
{
	dispatcher->removeListener(eventType, listener);
}

void InputManager::dispatch(IEvent* ev)
{
	dispatcher->dispatch(ev);
}

void InputManager::tick(float dt)
{
	dispatcher->tick(dt);
	inputHandler->tick(dt, dispatcher);
}

InputManager::InputManager() :
dispatcher(new Dispatcher()),
inputHandler(nullptr)
{ }