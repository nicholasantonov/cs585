#include "i-state.hh"

#include "dynamic-array.hh"
#include "state-transition.hh"

// Simple state that simply adds to the x cord of a node, made for testing
class AddState : public IState
{
public:
	AddState(int f)
	{
		factor = f;
	}

	virtual ~AddState(){}

	void tick(float, IActor* actor)
	{
		actor->update(actor->getX() + factor, actor->getY());
	}

	void transition(std::string newState)
	{
		TransitionEvent* ev = new TransitionEvent();
		ev->state = newState;
		machines->dispatch(ev);
	}

	void setUp(IActor* actor)
	{
		actor->update(actor->getX() + 1, actor->getY());
	}

	void breakDown(IActor* actor)
	{

	}

private:
	int factor;
};