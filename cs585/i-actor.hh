#ifndef _I_ACTOR_HH_
#define _I_ACTOR_HH_

#include <string>
#include "trie.hh"
#include "scene-node.hh"

class IActor : public SceneNode
{
public:
	Trie<float>* attributes;
	virtual std::string getDescription() = 0;
	virtual bool canBeInteractedWith() = 0;
	virtual void handleInput(int input, int inputX, int inputY) = 0;
	virtual std::string getType() = 0;
};

#endif