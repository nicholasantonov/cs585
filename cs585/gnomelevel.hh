#ifndef _GNOMELEVEL_HH_
#define _GNOMELEVEL_HH_

#include "i-level.hh"
#include "state-machine.hh"
#include "i-actor.hh"
#include "json.hh"
#include "dynamic-array.hh"
#include "actor-factory.hh"

class GnomeLevel : public ILevel
{
public:
	~GnomeLevel();
	void tick(float dt);
	void loadLevel(std::string directory);

	int getGold();
	void addGold(int g);
	IListener* getListener();

	StateMachine* stateMachine;
private:
	ActorFactory* factory;
	DynamicArray<std::string>* getMenu(Json*, std::string);
	IActor* selectedActor;
	int gold;

	class LevelListener : public IListener
	{
	private:
		GnomeLevel* instance;
	public:
		void setInstance(GnomeLevel* value);
		virtual void handle(IEvent*);
	} listener;
};

#endif