#include "leveled-building-function.hh"

#include "level-manager.hh"
#include "scene-manager.hh"
#include "to-string.hh"

#define ERROR_TIME 1.5f

LeveledBuildingFunction::LeveledBuildingFunction(Building* b) :
building(b)
{}

void LeveledBuildingFunction::handleInput(int input, int inputX, int inputY)
{
	if (input == 'u')
	{
		int cost = building->attributes->get("upgradecost");
		if (LevelManager::getInstance()->getGold() < cost)
		{
			SceneManager::getInstance()->displayMessage("Not enough gold", ERROR_TIME);
			return;
		}
		if (building->level >= 3)
		{
			SceneManager::getInstance()->displayMessage(building->type + " already at max level", ERROR_TIME);
			return;
		}

		// Upgrade the building
		LevelManager::getInstance()->addGold(-1 * cost);
		building->level++;
		SceneManager::getInstance()->displayMessage("Upgraded " + building->type + " to level " + toString(building->level) + "!", ERROR_TIME);
	}
}