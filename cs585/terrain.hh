#ifndef _TERRAIN_HH_
#define _TERRAIN_HH_

#define TERRAIN_LEVEL (1)
#define TERRAIN_COLLISION ((unsigned long)0b11)

#include "scene-node.hh"
#include "trie.hh"
#include "dispatcher.hh"
#include "i-tickable.hh"
#include "i-actor.hh"

class Terrain : public ITickable, public IActor
{
public:
	Terrain();
	Terrain(int xPos, int yPos, unsigned long collisionMap, IDrawable* d, std::string t);
	~Terrain();

	void dealDamage(float damage);
	void tick(float);
	std::string getDescription();
	bool canBeInteractedWith();
	void handleInput(int input, int inputX, int inputY);
	std::string getType();

	std::string type;

};

#endif