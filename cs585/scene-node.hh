#ifndef _SCENE_NODE_HH_
#define _SCENE_NODE_HH_

#include "i-drawable.hh"
#include <string>

class SceneNode
{
public:
	SceneNode();
	SceneNode(int xPos, int yPos);
	SceneNode(int xPos, int yPos, unsigned long collisionMap, IDrawable* d);

	unsigned long collisions();
	void update(int xPos, int yPos);
	int getX();
	int getY();
	IDrawable* drawable;

protected:
	int x;
	int y;
	unsigned long map;
};

#endif