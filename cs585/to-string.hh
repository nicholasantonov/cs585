#ifndef _TO_STRING_HH_
#define _TO_STRING_HH_

#include <string>

// To string method since g++ has a bug where to_string is missing
std::string toString(float f);

#endif