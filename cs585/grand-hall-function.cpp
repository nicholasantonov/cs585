#include "grand-hall-function.hh"

#include "actor-factory.hh"
#include "level-manager.hh"
#include "scene-manager.hh"
#include "to-string.hh"

#define ERROR_TIME 1.5f

GrandHallFunction::GrandHallFunction(Building* b, std::string d) :
	directory(d),
	building(b),
	factory(ActorFactory(d))
{}

void GrandHallFunction::handleInput(int input, int inputX, int inputY)
{
	if (input == 'u')
	{
		// Check to see if player has enough gold
		int cost = building->attributes->get("upgradecost");
		if (LevelManager::getInstance()->getGold() < cost)
		{
			SceneManager::getInstance()->displayMessage("Not enough gold", ERROR_TIME);
			return;
		}

		// Make sure the level isn't maxed already
		if (building->level >= 3)
		{
			SceneManager::getInstance()->displayMessage("Building already at max level", ERROR_TIME);
			return;
		}

		// Upgrade building
		LevelManager::getInstance()->addGold(-1 * cost);
		building->level++;
		SceneManager::getInstance()->displayMessage("Upgraded " + building->type + " to level " + toString(building->level) + "!", ERROR_TIME);

	}
	else if (input == 'r')
	{
		if (nullptr != (SceneManager::getInstance()->getSceneGraph()->getTopDrawableNodeAt(building->entranceX, building->entranceY)))
		{
			SceneManager::getInstance()->displayMessage("Unit blocking grand hall exit", ERROR_TIME);
			return;
		}
		int cost = building->attributes->get("recruitcost");
		if (LevelManager::getInstance()->getGold() < cost)
		{
			SceneManager::getInstance()->displayMessage("Not enough gold", ERROR_TIME);
			return;
		}
		LevelManager::getInstance()->addGold(-1 * cost);
		factory.createDwarf(building->entranceX, building->entranceY);
		SceneManager::getInstance()->displayMessage("Recruited a new dwarf!", ERROR_TIME);
	}
	else if (input == 'a')
	{
		// Check for previous apothecary
		int apothecariesBuilt = building->attributes->get("apothecariesbuilt");
		if (apothecariesBuilt)
		{
			SceneManager::getInstance()->displayMessage("You already built an apothecary", ERROR_TIME);
			return;
		}

		// Check to see if player has enough gold
		int cost = building->attributes->get("apothecarycost");
		if (LevelManager::getInstance()->getGold() < cost)
		{
			SceneManager::getInstance()->displayMessage("Not enough gold", ERROR_TIME);
			return;
		}

		// Spawn the new apothecary where the player's cursor is
		LevelManager::getInstance()->addGold(-1 * cost);
		factory.createApothecary(inputX, inputY);
		building->attributes->insert("apothecariesbuilt", ++apothecariesBuilt);	
		SceneManager::getInstance()->displayMessage("New apothecary built!", ERROR_TIME);
	}
	else if (input == 'b')
	{
		// Check for previous apothecary
		int blacksmithsBuilt = building->attributes->get("blacksmithsbuilt");
		if (blacksmithsBuilt)
		{
			SceneManager::getInstance()->displayMessage("You already built a blacksmith", ERROR_TIME);
			return;
		}

		// Check to see if player has enough gold
		int cost = building->attributes->get("blacksmithcost");
		if (LevelManager::getInstance()->getGold() < cost)
		{
			SceneManager::getInstance()->displayMessage("Not enough gold", ERROR_TIME);
			return;
		}

		// Spawn the new blacksmith where the player's cursor is
		LevelManager::getInstance()->addGold(-1 * cost);
		factory.createBlacksmith(inputX, inputY);
		building->attributes->insert("blacksmithsbuilt", ++blacksmithsBuilt);	
		SceneManager::getInstance()->displayMessage("New blacksmith built!", ERROR_TIME);
	}
}