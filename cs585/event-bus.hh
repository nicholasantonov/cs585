#ifndef _EVENT_BUS_HH_
#define _EVENT_BUS_HH_

#include "dispatcher.hh"

class EventBus
{
public:
	static EventBus* getInstance();

	void addListener(std::string eventType, IListener* listener);
	void removeListener(std::string eventType, IListener* listener);
	void dispatch(IEvent* ev);
	void tick(float dt);

private:
	EventBus();

	EventBus(EventBus const&);
	void operator=(EventBus const&);

	Dispatcher* dispatcher;

	static EventBus* instance;
};

#endif