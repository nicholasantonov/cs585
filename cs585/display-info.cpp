#include "display-info.hh"

#include "scene-manager.hh"
#include "i-actor.hh"

void displayInfoFromSelectedTile()
{
	int x = SceneManager::getInstance()->cursorX();
	int y = SceneManager::getInstance()->cursorY();
	IActor* actor = (IActor*)SceneManager::getInstance()->getSceneGraph()->getTopDrawableNodeAt(x, y);

	if (actor == nullptr)
	{
		SceneManager::getInstance()->closeStatus();
	}
	else
	{
		SceneManager::getInstance()->displayStatus(actor->getDescription());
	}
}