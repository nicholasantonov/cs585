#ifndef _TEST_HH_
#define _TEST_HH_

#include "dynamic-array.hh"
#include <string>

class Test
{
public:
	Test();
	Test(std::string name);
	Test(std::string name, bool failSetting);
	void define(std::string description);
	void exitOnFail(bool failSetting);
	void expect(bool result);
	void run();
protected:
	bool quitOnFail;
	int testIndex;
	DynamicArray<std::string>* descriptions;
	DynamicArray<bool>* results;

	void printTestCompletion(unsigned int testsPassed);
private:
	std::string testName;
};

#endif