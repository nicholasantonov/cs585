#include "dynamic-array.hh"
#include "stack.hh"
#include "queue.hh"
#include "test.hh"
#include "trie.hh"
#include "debug.hh"
#include "json.hh"
#include "generic-event.hh"
#include "error-test.hh"
#include "dispatcher.hh"
#include "scene-node.hh"
#include "state-machine.hh"
#include "add-state.hh"
#include "ncurses-input.hh"
#include "input-event.hh"
#include "event-bus.hh"
#include "game.hh"
#include "input-manager.hh"
#include <iostream>
#include <string>
#include <ncurses.h>




void printTestDebug()
{
	// All will print
	DEBUG_INITIALIZE();
	DEBUG_SET_LEVEL("log");
	DEBUG_UNMUTE("foo");
	DEBUG_LOG("foo", "a");
	DEBUG_WARN("foo", "b");
	DEBUG_ERROR("foo", "c");

	// a will not print
	DEBUG_SET_LEVEL("warn");
	DEBUG_LOG("foo", "a");
	DEBUG_WARN("foo", "b");
	DEBUG_ERROR("foo", "c");

	// Nothing will print since channel was muted
	DEBUG_MUTE("foo");
	DEBUG_LOG("foo", "a");
	DEBUG_WARN("foo", "b");
	DEBUG_ERROR("foo", "c");

	// Won't print due to bar not being unmuted
	DEBUG_LOG("bar", "a");
	DEBUG_WARN("bar", "b");
	DEBUG_ERROR("bar", "c");
	
	// warn and error will now print
	DEBUG_UNMUTE("bar");
	DEBUG_LOG("bar", "a");
	DEBUG_WARN("bar", "b");
	DEBUG_ERROR("bar", "c");

	DEBUG_SET_LEVEL("log");

}

// Runs unit tests to ensure that the Trie works
void testTrie()
{
	Test test = Test("Trie");
	Trie<std::string>* foo = new Trie<std::string>();

	test.define("Add and get with 1 char keys");
	foo->insert("a", "b");
	test.expect(foo->get("a") == "b");
	test.expect(foo->wasSet("a"));

	test.define("Add and get with normal keys");
	foo->insert("foo", "bar");
	test.expect(foo->get("foo") == "bar");
	test.expect(foo->wasSet("foo"));

	test.define("Add and get with keys that contain partial previous strings");
	foo->insert("ford", "car");
	test.expect(foo->get("ford") == "car");
	test.expect(foo->wasSet("ford"));

	test.define("Add and get where previous keys are substrings");
	foo->insert("food", "apple");
	test.expect(foo->get("food") == "apple");
	test.expect(foo->wasSet("food"));

	test.define("Add and get where keys are substrings of previous");
	foo->insert("f", "a");
	test.expect(foo->get("f") == "a");
	test.expect(foo->wasSet("f"));

	test.define("Get element that shouldn't exist");
	test.expect(foo->get("dig") == std::string());
	test.expect(!foo->wasSet("dig"));

	
	test.run();
}

// Runs unit tests to ensure that the queue works
void testQueue()
{
	Test test = Test("Queue");
	Queue<std::string>* foo = new Queue<std::string>(4);

	test.define("Initial Conditions");
	test.expect(foo->length() == 0);
	test.expect(foo->allocated() == 4);
	test.expect(foo->isEmpty());

	test.define("Pushing one element");
	foo->push("A");
	test.expect(foo->length() == 1);
	test.expect(foo->peek().compare("A") == 0);
	test.expect(!foo->isEmpty());

	test.define("Pushing second element");
	foo->push("B");
	test.expect(foo->length() == 2);

	test.define("Peeking at back element");
	test.expect(foo->peek().compare("A") == 0);

	test.define("Popping one element");
	test.expect(foo->pop().compare("A") == 0);
	test.expect(foo->length() == 1);
	test.expect(foo->peek().compare("B") == 0);

	test.define("Popping second element");
	test.expect(foo->pop().compare("B") == 0);
	test.expect(foo->length() == 0);

	test.define("Pushing to a cycle one element");
	foo->push("A");
	foo->push("B");
	foo->push("C");
	foo->push("D");
	test.expect(foo->peek().compare("A") == 0);
	test.expect(foo->length() == 4);
	test.expect(foo->allocated() == 4);

	test.define("Expanding the queue");
	foo->push("E");
	test.expect(foo->length() == 5);
	test.expect(foo->peek().compare("A") == 0);
	test.expect(foo->allocated() == 6);

	test.run();
}

// Runs unit tests to ensure that the stack is working correctly
void testStack()
{
	Test test = Test("Stack");
	Stack<int>* foo = new Stack<int>(4);

	test.define("Initial Conditions");
	test.expect(foo->length() == 0);
	test.expect(foo->allocated() == 4);
	test.expect(foo->isEmpty());

	test.define("Pushing one element");
	foo->push(1);
	test.expect(foo->length() == 1);
	test.expect(foo->peek() == 1);
	test.expect(!foo->isEmpty());

	test.define("Pushing second element");
	foo->push(2);
	test.expect(foo->length() == 2);
	test.expect(foo->peek() == 2);

	test.define("Popping elements");
	test.expect(foo->pop() == 2);
	test.expect(foo->length() == 1);
	test.expect(foo->peek() == 1);
	test.expect(foo->pop() == 1);
	test.expect(foo->length() == 0);
	test.expect(foo->isEmpty());

	test.define("Expanding with push");
	for (int i = 1; i < 8; i++)
	{
		foo->push(i);
	}
	test.expect(foo->allocated() == 9);
	test.expect(foo->peek() == 7);
	test.expect(foo->length() == 7);
	test.expect(!foo->isEmpty());

	test.define("Allocating more places");
	foo->allocate(1);
	test.expect(foo->allocated() == 10);
	test.expect(foo->peek() == 7);
	test.expect(foo->length() == 7);
	test.expect(!foo->isEmpty());

	test.define("Popping everything out");
	for (int i = 7; i > 0; i--)
	{
		test.expect(foo->pop() == i);
	}
	test.expect(foo->allocated() == 10);
	test.expect(foo->length() == 0);
	test.expect(foo->isEmpty());

	test.run();
}

// Runs unit tests to ensure that the dynamic array is working correctly
void testDynamicArray()
{
	Test test = Test("Dynamic Array");
	DynamicArray<int>* foo = new DynamicArray<int>();

	test.define("Initial Conditions");
	test.expect(foo->length() == 0);
	test.expect(foo->allocated() == 16);

	test.define("Pushing one element");
	foo->push(1);
	test.expect(foo->length() == 1);
	test.expect(foo->get(0) == 1);

	test.define("Pushing second element");
	foo->push(2);
	test.expect(foo->length() == 2);
	test.expect(foo->get(1) == 2);

	test.define("Unshifting an element");
	foo->unshift(3);
	test.expect(foo->length() == 3);
	test.expect(foo->get(0) == 3);
	test.expect(foo->get(1) == 1);
	test.expect(foo->get(2) == 2);

	test.define("Inserting an element");
	foo->insert(4, 1);
	test.expect(foo->length() == 4);
	test.expect(foo->get(0) == 3);
	test.expect(foo->get(1) == 4);
	test.expect(foo->get(2) == 1);
	test.expect(foo->get(3) == 2);

	test.define("Swapping");
	foo->swap(0, 2);
	foo->swap(0, 4);
	test.expect(foo->length() == 4);
	test.expect(foo->get(0) == 1);
	test.expect(foo->get(1) == 4);
	test.expect(foo->get(2) == 3);
	test.expect(foo->get(3) == 2);

	test.define("Presized Constructor");
	DynamicArray<int>* bar = new DynamicArray<int>(1);
	test.expect(bar->length() == 0);
	test.expect(bar->allocated() == 1);

	test.define("Expanding with push");
	bar->push(1);
	test.expect(bar->allocated() == 1);
	test.expect(bar->length() == 1);
	bar->push(2);
	test.expect(bar->allocated() == 2);
	test.expect(bar->length() == 2);
	test.expect(bar->get(0) == 1);
	test.expect(bar->get(1) == 2);

	test.define("Expanding with insert");
	bar->insert(3, 1);
	test.expect(bar->allocated() == 3);
	test.expect(bar->length() == 3);
	bar->insert(4, 2);
	test.expect(bar->allocated() == 4);
	test.expect(bar->length() == 4);
	bar->insert(5, 2);
	test.expect(bar->get(0) == 1);
	test.expect(bar->get(1) == 3);
	test.expect(bar->get(2) == 5);
	test.expect(bar->get(3) == 4);
	test.expect(bar->get(4) == 2);
	test.expect(bar->allocated() == 6);
	test.expect(bar->length() == 5);

	test.define("Expanding with unshift");
	for (int i = 6; i < 10; i++)
	{
		bar->unshift(i);
	}
	test.expect(bar->get(0) == 9);
	test.expect(bar->get(1) == 8);
	test.expect(bar->get(2) == 7);
	test.expect(bar->get(3) == 6);
	test.expect(bar->get(4) == 1);
	test.expect(bar->get(5) == 3);
	test.expect(bar->get(6) == 5);
	test.expect(bar->get(7) == 4);
	test.expect(bar->get(8) == 2);
	test.expect(bar->allocated() == 9);
	test.expect(bar->length() == 9);

	test.define("Allocating extra spaces");
	bar->allocate(9);
	test.expect(bar->get(0) == 9);
	test.expect(bar->get(1) == 8);
	test.expect(bar->get(2) == 7);
	test.expect(bar->get(3) == 6);
	test.expect(bar->get(4) == 1);
	test.expect(bar->get(5) == 3);
	test.expect(bar->get(6) == 5);
	test.expect(bar->get(7) == 4);
	test.expect(bar->get(8) == 2);
	test.expect(bar->allocated() == 18);
	test.expect(bar->length() == 9);

	test.define("Removing");
	bar->remove(0);
	test.expect(bar->get(0) == 8);
	test.expect(bar->get(1) == 7);
	test.expect(bar->get(2) == 6);
	test.expect(bar->get(3) == 1);
	test.expect(bar->get(4) == 3);
	test.expect(bar->get(5) == 5);
	test.expect(bar->get(6) == 4);
	test.expect(bar->get(7) == 2);
	test.expect(bar->allocated() == 18);
	test.expect(bar->length() == 8);
	bar->remove(7);
	test.expect(bar->get(0) == 8);
	test.expect(bar->get(1) == 7);
	test.expect(bar->get(2) == 6);
	test.expect(bar->get(3) == 1);
	test.expect(bar->get(4) == 3);
	test.expect(bar->get(5) == 5);
	test.expect(bar->get(6) == 4);
	test.expect(bar->allocated() == 18);
	test.expect(bar->length() == 7);
	bar->remove(15);
	bar->remove(99);
	test.expect(bar->allocated() == 18);
	test.expect(bar->length() == 7);
	for (unsigned int i = 0; i < 5; i++)
	{
		bar->remove(1);
	}
	test.expect(bar->allocated() == 18);
	test.expect(bar->length() == 2);
	test.expect(bar->get(0) == 8);
	test.expect(bar->get(1) == 4);
	for (unsigned int i = 0; i < 3; i++)
	{
		bar->remove(0);
	}

	test.run();
}

// Runs unit tests to make sure that c++11 is set up correctly
void testCppEleven()
{
	Test test = Test("C++ 11");
	std::string s;

	test.define("C++ 11 properly set up");
	s = "123";
	s.pop_back();
	test.expect(s.compare("12") == 0);

	/*test.define("C++ 11 stoi works");
	test.expect((std::stoi(s) - 2) == 10);*/

	test.run();
}

// Runs unit tests to make sure the json parser is working correctly
void testJson()
{

	Test test = Test("Json");
	Json* foo = new Json();

	test.define("Parse simple object");
	foo->parse("{ \"name\" : \"tom\" }");
	test.expect(foo->getMember("name")->getString().compare("tom") == 0);

	Json* a = new Json();
	test.define("Parse an array with length 1");
	a->parse("[\n		\"json/dwarves1/\"\n	]");
	test.expect(a->get(0)->getString().compare("json/dwarves1/") == 0);

	Json* hoo = new Json();
	test.define("Parse simple object with multiple values per object");
	hoo->parse("{ \"name\" : \"tom\", \"wallet\"  : \"stolen\" }");
	test.expect(hoo->getMember("name")->getString().compare("tom") == 0);
	test.expect(hoo->getMember("wallet")->getString().compare("stolen") == 0);

	Json* goo = new Json();
	test.define("Parse simple object with multiple levels and nesting");
	goo->parse("{ \"name\" : \"tom\", \"wallet\"  : {\"dollars\" : \"none\"} }");
	test.expect(goo->getMember("name")->getString().compare("tom") == 0);
	test.expect(goo->getMember("wallet")->getMember("dollars")->getString().compare("none") == 0);

	Json* i = new Json();
	test.define("Parse simple object with multiple levels and nesting numbers");
	i->parse("{ \"name\" : \"tom\", \"wallet\"  : {\"dollars\" :  35 } }");
	test.expect(i->getMember("name")->getString().compare("tom") == 0);

	Json* j = new Json();
	test.define("Parse simple object with multiple values per object");
	j->parse("{ \"name\" : \"tom\", \"wallet\"  : [\"dollah\", \"dough\"] }");
	test.expect(j->getMember("name")->getString().compare("tom") == 0);
	test.expect(j->getMember("wallet")->get(0)->getString().compare("dollah") == 0);
	
	Json* k = Json::parseFile("jsontest.json");
	test.define("Parse from a file");
	test.expect(k->getMember("name")->getString().compare("tom") == 0);
	test.expect(k->getMember("wallet")->get(0)->getString().compare("dollah") == 0);

	Json* l = new Json();
	test.define("Parse Numbers in an array");
	l->parse("[4.5, 3, 4]");
	test.expect(l->get(0)->getNumber() == 4.5);
	test.expect(l->get(1)->getNumber() == 3);
	test.expect(l->get(2)->getNumber() == 4);


	test.run();
}

// Runs unit tests to ensure that the event-listener dispatch system is working correctly
void testEvents()
{
	Test test = Test("Event");
	int num = 0;

	test.define("Listener registers and handles event");
	Dispatcher* dispatcher = new Dispatcher();
	ErrorTest* errorTest = new ErrorTest(&num, dispatcher, "trigger");
	dispatcher->tick(0.01f); // tick to make sure listener gets added
	dispatcher->dispatch(new GenericEvent("trigger"));
	dispatcher->tick(0.01f);
	test.expect(num == 1);
	errorTest->resetNum();
	test.expect(num == 0);

	test.define("Dummy events trigger nothing");
	dispatcher->dispatch(new GenericEvent("trig"));
	dispatcher->tick(0.01f);
	dispatcher->dispatch(new GenericEvent("dig"));
	dispatcher->tick(0.01f);
	test.expect(num == 0);

	test.define("Listener gets removed correctly");
	dispatcher->removeListener("trigger", errorTest->getListener());
	dispatcher->dispatch(new GenericEvent("trigger"));
	dispatcher->tick(0.01f);
	test.expect(num == 1);
	errorTest->resetNum();
	test.expect(num == 0);

	test.define("Listener got removed at the end of ticks");
	dispatcher->dispatch(new GenericEvent("trigger"));
	dispatcher->tick(0.01f);
	test.expect(num == 0);



	test.run();
}

// Runs unit tests to ensure that the state machine is functioning correctly
void testStateMachine()
{
	Test test = Test("State Machine");
	test.define("Stateless machine constructs and ticks correctly");
	SceneNode* node = new SceneNode(500, 500);
	AddState* adder = new AddState(5);
	AddState* subber = new AddState(-1);
	Trie<IState*>* lookup = new Trie<IState*>();
	lookup->insert("add", adder);
	lookup->insert("sub", subber);

	StateMachine* machine = new StateMachine(lookup, "add", (IActor*)node);
	machine->tick(0.1f);
	test.expect((node->getX() == 506) && (node->getY() == 500));

	test.define("state machine transitions and ticks correctly with new state");
	adder->transition("sub");
	// There is one tick of lag for the dispatcher to dispatch the events
	// Grapsas says this is fine
	machine->tick(0.1f);
	machine->tick(0.1f);
	test.expect((node->getX() == 511) && (node->getY() == 500));
	


	test.run();
}

// Sample program to print out input
void testInput()
{
	initscr();
	noecho();
	InputManager::getInstance()->setInputHandler(new NcursesInput());
	class PrintKey : public IListener
	{
	public:
		void handle(IEvent* e)
		{
			int key = ((InputEvent*)e)->input;

			if (key == InputEvent::UP)
			{
				std::cout << "up " << std::flush;
			}
			else if (key == InputEvent::DOWN)
			{
				std::cout << "down " << std::flush;
			}
			else if (key == InputEvent::LEFT)
			{
				std::cout << "left " << std::flush;
			}
			else if (key == InputEvent::RIGHT)
			{
				std::cout << "right " << std::flush;
			}
			else if (key == ' ')
			{
				std::cout << "space " << std::flush;
			}
			else
			{
				std::cout << (char)key << " " << std::flush;
			}
		}
	} _onKey;

	InputManager::getInstance()->addListener("input", &_onKey);

	while (1)
	{
		InputManager::getInstance()->tick(0.1f);
	}
}

int main(int argumentLength, char** argumentString)
{
	if ((argumentLength > 1) && (*argumentString[1] == 't'))
	{
		printTestDebug();
		testDynamicArray();
		testStack();
		testQueue();
		testTrie();
		//testCppEleven();
		testEvents();
		testStateMachine();
		testJson();
		//testInput();
	}
	else
	{
		DEBUG_INITIALIZE();
		std::cout << "GAME LOADING" << std::endl;

		Game* game = new Game();

		game->run();
	}
	std::cout << "GAME EXITING" << std::endl;
}