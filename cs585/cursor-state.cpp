#include "cursor-state.hh"

#include "input-event.hh"
#include "input-manager.hh"
#include "scene-manager.hh"
#include "to-string.hh"
#include "i-actor.hh"
#include "display-info.hh"

CursorState::CursorState()
{
	_onKey.instance = this;
}

void CursorState::tick(float dt, IActor* actor)
{
	displayInfoFromSelectedTile();
}

void CursorState::setUp(IActor* actor)
{
	InputManager::getInstance()->addListener("input", &_onKey);
}

void CursorState::breakDown(IActor* actor)
{
	InputManager::getInstance()->removeListener("input", &_onKey);
	SceneManager::getInstance()->closeStatus();
}

void CursorState::OnKey::handle(IEvent* e)
{
	int key = ((InputEvent*)e)->input;
	int x = SceneManager::getInstance()->cursorX();
	int y = SceneManager::getInstance()->cursorY();
	int maxX = SceneManager::getInstance()->getSceneGraph()->getBounds().maxX;
	int maxY = SceneManager::getInstance()->getSceneGraph()->getBounds().maxY;

	if (key == InputEvent::UP && y > 0)
	{
		y -= 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == InputEvent::DOWN && y < maxY)
	{
		y += 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == InputEvent::LEFT && x > 0)
	{
		x -= 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == InputEvent::RIGHT && x < maxX)
	{
		x += 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == ' ')
	{
		IActor* actor = (IActor*)SceneManager::getInstance()->getSceneGraph()->getTopDrawableNodeAt(x, y);
		// if the current position has an interactable actor
		if (actor != nullptr && actor->canBeInteractedWith())
		{
			*(((CursorState*)instance)->levelSelectedActor) = actor;
			instance->transition("interact");
		}
		else
		{
			instance->transition("camera");
		}
	}
}