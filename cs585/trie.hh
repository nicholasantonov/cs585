#ifndef _TRIE_HH_
#define _TRIE_HH_

#include <string>

template <class Elem>
class Trie
{
public:
	// Default constructor
	Trie()
	{
		root = new Node();
	}

	// Default Deconstructor
	~Trie()
	{
		delete root;
	}

	// Sets the value associated with a given key
	void insert(std::string key, Elem element)
	{
		Node* node = getNode(key);
		node->data = element;
		node->set = true;
	}

	// Gets the value that is associated with a given key
	Elem& get(std::string key)
	{
		Elem& ret = getNode(key)->data;
		return ret;
	}

	bool wasSet(std::string key)
	{
		return getNode(key)->set;
	}

private:
	class Node
	{
	public:
		Node()
		{
			children = new Node*[26];
			for (unsigned short i = 0; i < 26; i++)
			{
				children[i] = nullptr;
			}
			set = false;
		}

		Node(char letter)
		{
			id = letter;
			children = new Node*[26];
			for (unsigned short i = 0; i < 26; i++)
			{
				children[i] = nullptr;
			}
			set = false;
		}

		~Node()
		{
			delete children;
		}

		// Gets a child that corresponds to a certain letter, or creates one if none exists
		Node* child(char letter)
		{
			if (children[letter - 'a'] != nullptr)
			{
				return children[letter - 'a'];
			}
			return addChild(new Node(letter));
		}

		// Adds a node as a child and returns ta pointer to it
		Node* addChild(Node* child)
		{
			children[child->id - 'a'] = child;
			return child;
		}

		char id;
		Elem data;
		bool set;
	protected:
		Node** children;
	};

	Node* root;

	Node* getNode(std::string key)
	{
		Node* node = root;
		for (unsigned short i = 0; i < key.length(); i++)
		{
			node = node->child(key[i]);
		}
		return node;
	}
};
#endif