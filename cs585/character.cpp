#include "character.hh"

#include "generic-event.hh"
#include "to-string.hh"
#include "state-transition.hh"
#include "death-event.hh"

Character::Character() :
maxHealth(100.0f)
{
	x = 0;
	y = 0;
	map = 0;
	attributes = new Trie<float>;
	dispatcher = new Dispatcher();
	health = 100.0f;
	currentBuilding = nullptr;
}

Character::Character(int xPos, int yPos, unsigned long collisionMap, IDrawable* d, float h) :
maxHealth(h)
{
	drawable = d;
	x = xPos;
	y = yPos;
	map = collisionMap;
	attributes = new Trie<float>;
	health = h;
	dispatcher = new Dispatcher();
	currentBuilding = nullptr;
	target = nullptr;
}

Character::~Character()
{
	delete attributes;
	delete drawable;
}

void Character::addListener(std::string eventType, IListener* listener)
{
	dispatcher->addListener(eventType, listener);
}

void Character::removeListener(std::string eventType, IListener* listener)
{
	dispatcher->removeListener(eventType, listener);
}

void Character::handleInput(int input, int inputX, int inputY)
{
	if (input == 'm')
	{
		attributes->insert("targetx", inputX);
		attributes->insert("targety", inputY);
		dispatcher->dispatch(new TransitionEvent("move"));
	}
}

void Character::dealDamage(float damage, Character* attacker)
{
	health -= damage;
	if (health < 0)
	{
		health = 0;
		dispatcher->dispatch(new DeathEvent(this, attacker));
	}
}

std::string Character::getDescription()
{
	std::string description = type + " " + toString(id);
	description += " | health: " + toString(health);
	description += " | gold: " + toString(attributes->get("gold"));
	description += " | awakeness " + toString((int)attributes->get("sleepmeter"));
	description += " | happiness " + toString((int)attributes->get("thirstmeter"));

	return description;
}

bool Character::canBeInteractedWith()
{
	return isMoveable;
}

std::string Character::getType()
{
	return type;
}

void Character::simulateNeeds(float dt)
{
	if (type.compare("dwarf") == 0)
	{
		attributes->get("thirstmeter") -= attributes->get("thirstrate") * dt;
		if (attributes->get("thirstmeter") < 0)
		{
			attributes->insert("thirstmeter", 0.0f);
		}

		attributes->get("sleepmeter") -= attributes->get("sleeprate") * dt;
		if (attributes->get("sleepmeter") < 0)
		{
			attributes->insert("sleepmeter", 0.0f);
		}
	}
}