#ifndef _BUILDING_HH_
#define _BUILDING_HH_

#include "i-actor.hh"
#include "i-function.hh"
#include "dispatcher.hh"

#define BUILDING_COLLISON ((unsigned long)0b11)
#define BUILDING_LEVEL 6

class Building : public IActor
{
public:
	Building(int xPos, int yPos, int size, unsigned long collisionMap, IDrawable* d);

	std::string getDescription();
	void dealDamage(float);
	bool canBeInteractedWith();
	void handleInput(int input, int inputX, int inputY);
	std::string getType();

	int entranceX;
	int entranceY;
	int size;
	int level;
	Dispatcher* dispatcher;
	std::string type;
	IFunction* function;
};

#endif