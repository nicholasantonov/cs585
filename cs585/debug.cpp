#include "trie.hh"
#include <iostream>
#include <string>
#include <ctime>

#include "debug.hh"

Debug* Debug::getInstance()
{
	if (instance == nullptr)
	{
		instance = new Debug();
	}
	return instance;
}

void Debug::log(std::string channel, std::string message)
{
#ifndef _DISABLE_DEBUG_
	msg(channel, message, LOG_LEVEL);
#endif
}

void Debug::warn(std::string channel, std::string message)
{
#ifndef _DISABLE_DEBUG_
	msg(channel, message, WARN_LEVEL);
#endif
}

void Debug::error(std::string channel, std::string message)
{
#ifndef _DISABLE_DEBUG_
	msg(channel, message, ERROR_LEVEL);
#endif
}

void Debug::mute(std::string channel)
{
#ifndef _DISABLE_DEBUG_
	channels->insert(channel, false);
#endif
}

void Debug::unMute(std::string channel)
{
#ifndef _DISABLE_DEBUG_
	channels->insert(channel, true);
#endif
}

//Set the filter for what should be printed
void Debug::setLevel(std::string level)
{
#ifndef _DISABLE_DEBUG_
	/*
	* 2 prints everything, log level
	* 1 prints warns and errors
	* 0 prints only errors
	*/
	if (!level.compare("log"))
	{
		mode = LOG_LEVEL;
	}
	else if (!level.compare("warn"))
	{
		mode = WARN_LEVEL;
	}
	else if (!level.compare("log"))
	{
		mode = ERROR_LEVEL;
	}
#endif
}

// Useless method that prints date for assignment reqs
void Debug::initialize()
{
#ifndef _DISABLE_DEBUG_
	time_t now = time(0);
	std::string currentTime = ctime(&now);
	currentTime.erase(currentTime.length() - 1);
	std::cout << currentTime << std::endl;
#endif
}

Debug::Debug()
{
	mode = (unsigned short)2;
	channels = new Trie<bool>();
}

Debug::Debug(Debug const&)
{
}


void Debug::msg(std::string channel, std::string message, int level)
{
	// If the correct method for the logging level was called and the channel is not muted
	if ((level <= mode) && channels->wasSet(channel) && channels->get(channel))
	{
		//Format time string to display current time nicely
		time_t now = time(0);
		std::string currentTime = ctime(&now);
		//currentTime.pop_back();
		currentTime.erase(0, 11);
		currentTime.erase(8, 6);

		message = channel + "::" + currentTime + "::" + message;

		// Call log methods here
		logTerminal(message);
	}
}

// Standard terminal logging
void Debug::logTerminal(std::string message)
{
	std::cout << message << std::endl;
}


Debug::~Debug()
{
	delete channels;
}

Debug* Debug::instance = nullptr;