#include "i-listener.hh"
#include "dispatcher.hh"

// Class made specifically to test events
class ErrorTest
{
private:
	int* mainNumber;

	class OnError : public IListener
	{
	private:
		ErrorTest* _instance;
	public:
		OnError() { }

		void setInstance(ErrorTest* value) { _instance = value; }

		virtual void handle(IEvent*)
		{
			*(_instance->mainNumber) = 1;
		}
	} _onError;
public:
	ErrorTest(int* n, Dispatcher* dispatcher, std::string type)
	{
		mainNumber = n;
		_onError.setInstance(this);
		dispatcher->addListener(type, &_onError);
	}

	void resetNum()
	{
		*mainNumber = 0;
	}

	OnError* getListener()
	{
		return &_onError;
	}
};