#include "ncurses-input.hh"

#include "input-manager.hh"
#include "input-event.hh"
#include "debug.hh"
#include <stdio.h>
#include <ncurses.h>

NcursesInput::NcursesInput(){}

void NcursesInput::tick(float, Dispatcher* d)
{
	int c = getch();
	if (c != ERR)
	{
		DEBUG_LOG("input", "sending input event");
		d->dispatch(new InputEvent(c));
	}
}