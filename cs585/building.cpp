#include "building.hh"

#include "i-function.hh"
#include "to-string.hh"

Building::Building(int xPos, int yPos, int sz, unsigned long collisionMap, IDrawable* d)
{
	size = sz;
	drawable = d;
	x = xPos;
	y = yPos;
	map = collisionMap;
	attributes = new Trie<float>;
	dispatcher = new Dispatcher();
	level = 1;
	type = "default";
}

std::string Building::getDescription()
{
	std::string ret = "A level ";
	ret += toString(level);
	ret += " ";
	ret += type;
	return ret;//"A level " + toString(level) + " " + type;
}

void Building::dealDamage(float)
{
	// TODO destructible buildings
}

bool Building::canBeInteractedWith()
{
	return true;
}

void Building::handleInput(int input, int inputX, int inputY)
{
	function->handleInput(input, inputX, inputY);
}

std::string Building::getType()
{
	return type;
}