#ifndef _DEBUG_HH_
#define _DEBUG_HH_

#define LOG_LEVEL 2
#define WARN_LEVEL 1
#define ERROR_LEVEL 0
//#define _DISABLE_DEBUG_
#include "trie.hh"
#include <string>

/*
 * Debug Class
 * Can log, warn, and error to any channel using the preproccessor definitions
 * Any channel will accept logs, but only will print if specifically unmuted
 * Only messages fitting the specified detail level will print
 * The entire system can be disable by defining _DISABLE_DEBUG_ before including this header
 */
class Debug
{
public:
	static Debug* getInstance();

	void log(std::string channel, std::string message);

	void warn(std::string channel, std::string message);

	void error(std::string channel, std::string message);

	void mute(std::string channel);

	void unMute(std::string channel);

	//Set the filter for what should be printed
	void setLevel(std::string level);

	// Useless method that prints date for assignment reqs
	void initialize();

protected:
	Debug(Debug const&);

	Debug& operator=(Debug const&);

	void msg(std::string channel, std::string message, int level);

	// Standard terminal logging
	void logTerminal(std::string message);

	Debug();

	~Debug();

	unsigned short mode;

	Trie<bool>* channels;

	static Debug* instance;
};

#define DEBUG_LOG			Debug::getInstance()->log
#define DEBUG_WARN			Debug::getInstance()->warn
#define DEBUG_ERROR			Debug::getInstance()->error
#define DEBUG_SET_LEVEL		Debug::getInstance()->setLevel
#define DEBUG_MUTE			Debug::getInstance()->mute
#define DEBUG_UNMUTE		Debug::getInstance()->unMute
#define DEBUG_INITIALIZE	Debug::getInstance()->initialize

#endif
