#include "interact-state.hh"

#include "dynamic-array.hh"
#include "input-event.hh"
#include "input-manager.hh"
#include "scene-manager.hh"
#include "level-manager.hh"
#include "to-string.hh"
#include <string>

InteractState::InteractState(Trie<DynamicArray<std::string>*>* m)
{
	menus = m;
	_onKey.instance = this;
}

void InteractState::tick(float dt, IActor* actor)
{
	SceneManager::getInstance()->displayStatus("Gold: " + toString(LevelManager::getInstance()->getGold()));
}

void InteractState::setUp(IActor* actor)
{
	SceneManager::getInstance()->displayMenu(menus->get((*levelSelectedActor)->getType()));
	InputManager::getInstance()->addListener("input", &_onKey);
}

void InteractState::breakDown(IActor* actor)
{
	SceneManager::getInstance()->closeMenu();
	InputManager::getInstance()->removeListener("input", &_onKey);
}

void InteractState::OnKey::handle(IEvent* e)
{
	int key = ((InputEvent*)e)->input;
	int x = SceneManager::getInstance()->cursorX();
	int y = SceneManager::getInstance()->cursorY();
	
	if (key == InputEvent::UP)
	{
		y -= 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == InputEvent::DOWN)
	{
		y += 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == InputEvent::LEFT)
	{
		x -= 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == InputEvent::RIGHT)
	{
		x += 1;
		SceneManager::getInstance()->moveCursor(x, y);
	}
	else if (key == ' ')
	{
		instance->transition("camera");
	}
	else
	{
		(*(((InteractState*)instance)->levelSelectedActor))->handleInput(key, x, y);
	}
}
