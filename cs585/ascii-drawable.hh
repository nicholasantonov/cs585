#ifndef _ASCII_DRAWABLE_HH_
#define _ASCII_DRAWABLE_HH_

#include "i-drawable.hh"

typedef struct t_rgb
{
	unsigned short red;
	unsigned short green;
	unsigned short blue;
} t_rgb;

class AsciiDrawable : public IDrawable
{
public:
	AsciiDrawable();
	AsciiDrawable(int, char, t_rgb);

	char icon;
	t_rgb colors;
};
#endif