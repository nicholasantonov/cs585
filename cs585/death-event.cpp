#include "death-event.hh"

DeathEvent::DeathEvent(Character* dead, Character* kill) :
	dier(dead),
	killer(kill)
{}

std::string DeathEvent::getType()
{
	return "death";
}