#ifndef _JSON_HH_
#define _JSON_HH_

#include "dynamic-array.hh"
#include "debug.hh"
#include "trie.hh"
#include <string>

class Json
{
public:
	Json();

	static Json* parseFile(std::string);

	bool parse(std::string string);

	Json* get(unsigned int index);
	DynamicArray<Json*>* getArray();
	float getNumber();
	std::string getString();
	Json* getMember(std::string string);

	DynamicArray<Json*>* arr;
	Trie<Json*>* members;
	bool boolean;
	float number;
	std::string dstring;

	enum t_type {
		NUMBER,
		STRING,
		ARRAY,
		OBJECT,
		BOOLEAN,
	} type;

	
	
private:

	std::string parseString(std::string string, unsigned int* i);
	void parseObject(std::string string, unsigned int* i);
	std::string getName(std::string string, unsigned int* i);
	Json* parseValue(std::string string, unsigned int* i);
	std::string parseNumber(std::string string, unsigned int* i);
	DynamicArray<Json*>* parseArray(std::string string, unsigned int* i);
};

#endif