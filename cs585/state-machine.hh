#ifndef _STATE_MACHINE_HH_
#define _STATE_MACHINE_HH_

#include "i-tickable.hh"
#include "i-state.hh"
#include "trie.hh"
#include "i-listener.hh"
#include "i-actor.hh"
#include "dynamic-array.hh"

#define STATE_TRANSITION_NAME "transition"

class StateMachine : public ITickable
{
public:
	StateMachine(Trie<IState*>*, std::string, IActor*);
	virtual ~StateMachine();
	virtual void tick(float dt);
	IListener* getListener();
private:
	Trie<IState*>* stateLookUp;
	IState* currentState;
	IActor* actor;
	class OnStateTransition : public IListener
	{
	private:
		StateMachine* instance;
	public:
		OnStateTransition();
		void setInstance(StateMachine* value);
		virtual void handle(IEvent*);
	} _onStateTransition;
};

#endif