#include "camera-state.hh"

#include "input-event.hh"
#include "input-manager.hh"
#include "scene-manager.hh"
#include "level-manager.hh"
#include "to-string.hh"

CameraState::CameraState()
{
	_onKey.instance = this;
}

void CameraState::tick(float dt, IActor* actor)
{
	SceneManager::getInstance()->displayStatus("Gold: " + toString(LevelManager::getInstance()->getGold()));
	SceneManager::getInstance()->tick(dt);
}

void CameraState::setUp(IActor* actor)
{
	InputManager::getInstance()->addListener("input", &_onKey);
}

void CameraState::breakDown(IActor* actor)
{
	InputManager::getInstance()->removeListener("input", &_onKey);
	SceneManager::getInstance()->closeStatus();
}

void CameraState::OnKey::handle(IEvent* e)
{
	int key = ((InputEvent*)e)->input;
	int x = SceneManager::getInstance()->cameraX();
	int y = SceneManager::getInstance()->cameraY();
	int cursor_x = SceneManager::getInstance()->cursorX();
	int cursor_y = SceneManager::getInstance()->cursorY();
	int max_x = SceneManager::getInstance()->getSceneGraph()->getBounds().maxX;
	int max_y = SceneManager::getInstance()->getSceneGraph()->getBounds().maxY;


	// move the camera according to the input, then move the cursor so it "follows the screen"
	// to disable following behavior, dont call move cursor, it will stay over what was selected before
	if (key == InputEvent::UP && y > 0)
	{
		SceneManager::getInstance()->moveCamera(x, y - 1);
		SceneManager::getInstance()->moveCursor(cursor_x, cursor_y - 1);
	}
	else if (key == InputEvent::DOWN && max_y > SceneManager::getInstance()->cameraY())
	{
		SceneManager::getInstance()->moveCamera(x, y + 1);
		SceneManager::getInstance()->moveCursor(cursor_x, cursor_y + 1);
	}
	else if (key == InputEvent::LEFT && x > 0)
	{
		SceneManager::getInstance()->moveCamera(x - 1, y);
		SceneManager::getInstance()->moveCursor(cursor_x - 1, cursor_y);
	}
	else if (key == InputEvent::RIGHT && max_x > SceneManager::getInstance()->cameraX())
	{
		SceneManager::getInstance()->moveCamera(x + 1, y);
		SceneManager::getInstance()->moveCursor(cursor_x + 1, cursor_y);
	}
	else if (key == ' ')
	{
		instance->transition("cursor");
	}
}