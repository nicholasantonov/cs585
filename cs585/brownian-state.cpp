#include "brownian-state.hh"

#include "scene-manager.hh"
#include "i-actor.hh"
#include "character.hh"
#include <cstdlib>

BrownianState::BrownianState()
{
	
}

void BrownianState::tick(float dt, IActor* a)
{
	Character* actor = (Character*)a;
	actor->simulateNeeds(dt);
	actor->attributes->get("timesincelastmove") += dt;
	if (actor->attributes->get("timesincelastmove") > actor->attributes->get("movedelay"))
	{
		actor->attributes->get("timesincelastmove") -= actor->attributes->get("movedelay");

		int x = actor->getX();
		int y = actor->getY();

		do
		{
			// Randomly choose an adjacent location
			x += (rand() % 3) - 1;
			y += (rand() % 3) - 1;

		} while (!(SceneManager::getInstance()->canMoveTo(actor, x, y)));

		SceneManager::getInstance()->getSceneGraph()->updateSceneNode(actor, x, y);
	}

	// Check for aggro
	std::string enemy;
	if (actor->type.compare("dwarf") == 0)
	{
		enemy = "orc";
	}
	else if (actor->type.compare("orc") == 0)
	{
		enemy = "dwarf";
	}

	IActor* unit;
	for (int x = actor->getX() - 1; x <= actor->getX() + 1; x++)
	{
		for (int y = actor->getY() - 1; y <= actor->getY() + 1; y++)
		{
			unit = (IActor*)SceneManager::getInstance()->getTopDrawableNodeAt(x, y);
			if (unit != nullptr && unit->getType().compare(enemy) == 0)
			{
				actor->target = (Character*)unit;
				transition("attack");
				return;
			}
		}
	}

	// check for things to do
	if (actor->type.compare("dwarf") == 0)
	{
		if (actor->attributes->get("thirstmeter") < actor->attributes->get("drinkthreshold"))
		{

		}
		else if (actor->attributes->get("sleepmeter") < actor->attributes->get("sleepthreshold"))
		{

		}
	}
}

void BrownianState::setUp(IActor* actor)
{
	
}

void BrownianState::breakDown(IActor* actor)
{
	
}