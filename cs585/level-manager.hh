#ifndef _LEVEL_MANAGER_HH_
#define _LEVEL_MANAGER_HH_

#include "i-level.hh"
#include "ncurses-input.hh"
#include "i-renderer.hh"
#include "i-listener.hh"
#include <string>

class LevelManager
{
public:
	static LevelManager* getInstance();

	void tick(float dt);

	

	void loadLevel(std::string directory);
	int getGold();
	void addGold(int g);
	IListener* getListener();

private:
	LevelManager();

	LevelManager(LevelManager const&);
	void operator=(LevelManager const&);

	void setLevel(ILevel* level);

	ILevel* level;
	NcursesInput* input;
	IRenderer* renderer;

	static LevelManager* instance;
};

#endif