#ifndef _I_LEVEL_HH_
#define _I_LEVEL_HH_

#include "i-tickable.hh"
#include "i-listener.hh"
#include <string>

class ILevel : public ITickable
{
public:
	virtual ~ILevel(){}
	virtual void loadLevel(std::string directory) = 0;
	virtual int getGold() = 0;
	virtual void addGold(int g) = 0;
	virtual IListener* getListener() = 0;
	int render_x;
	int render_y;
};

#endif