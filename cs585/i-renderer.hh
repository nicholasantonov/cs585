#ifndef _I_RENDERER_HH_
#define _I_RENDERER_HH_

#include "i-drawable.hh"
#include "dynamic-array.hh"

class IRenderer
{
public:
	virtual ~IRenderer(){}
	virtual void tick(float dt) = 0;
	virtual void setCamera(int start_x, int stary_y) = 0;
	virtual void moveCursor(int x, int y) = 0;
	virtual void hideCursor() = 0;
	virtual void showCursor() = 0;
	virtual void displayMessage(std::string, float) = 0;
	virtual void displayMenu(DynamicArray<std::string>*) = 0;
	virtual void closeMenu() = 0;
	virtual void displayStatus(std::string) = 0;
	virtual void closeStatus() = 0;
	float cursorX;
	float cursorY;
};

#endif