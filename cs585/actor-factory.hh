#ifndef _ACTOR_FACTORY_HH_
#define _ACTOR_FACTORY_HH_

#include <string>
#include "json.hh"
#include "building.hh"
#include "character.hh"
#include "dynamic-array.hh"
#include "state-machine.hh"

class ActorFactory
{
public:
	ActorFactory(std::string directory);
	void createDwarf(int x, int y);
	void createOrc(int x, int y);
	void createTree(int x, int y);
	void createMountain(int x, int y);
	void createGrandHall(int x, int y);
	void createApothecary(int x, int y);
	void createBlacksmith(int x, int y);
	void deleteCharacter(Character* character);

private:
	void buildWalls(Building* b);

	int charactersMade;
	std::string directory;
	Json* dwarfJson;
	Json* orcJson;
	Json* terrainJson;
	Json* grandHallJson;
	Json* apothecaryJson;
	Json* blacksmithJson;

	DynamicArray<StateMachine*>* characterControllers;
};

#endif