#include "listener-state.hh"

#include "input-event.hh"
#include "input-manager.hh"
#include "scene-manager.hh"

ListenerState::ListenerState(IListener* l) :
listener(l)
{}

void ListenerState::tick(float dt, IActor* actor)
{
	
}

void ListenerState::setUp(IActor* actor)
{
	InputManager::getInstance()->addListener("input", listener);
}

void ListenerState::breakDown(IActor* actor)
{
	InputManager::getInstance()->removeListener("input", listener);
}