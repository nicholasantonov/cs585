#ifndef _FIXED_GRID_HH_
#define _FIXED_GRID_HH_

#include "i-scene-graph.hh"
#include "scene-node.hh"
#include "dynamic-array.hh"
#include "i-drawable.hh"

class FixedGrid : public ISceneGraph
{
public:
	FixedGrid(int, int);
	virtual ~FixedGrid();
	void addSceneNode(SceneNode* node);
	void removeSceneNode(SceneNode* node);
	void updateSceneNode(SceneNode* node, int x, int y);
	bool canMoveTo(SceneNode* node, int x, int y);
	DynamicArray<SceneNode*>* getColliders(SceneNode* node);
	DynamicArray<SceneNode*>* getColliders(int x, int y);
	DynamicArray<SceneNode*>* getColliders(int x, int y, int radius);
	DynamicArray<SceneNode*>* getColliders(int cornerX1, int cornerY1, int cornerX2, int cornerY2);
	t_coordSet getBounds();
	SceneNode* getTopDrawableNodeAt(int x, int y);
	void setDefaultTile(SceneNode* node);

private:
	int xdim;
	int ydim;
	DynamicArray<SceneNode*>** positions;

	DynamicArray<SceneNode*>* nodesAt(int x, int y);
};

#endif