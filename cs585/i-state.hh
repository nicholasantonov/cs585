#ifndef _I_STATE_HH_
#define _I_STATE_HH_

#include "i-actor.hh"
#include "dispatcher.hh"

class IState
{
public:
	IState();
	virtual ~IState();
	void subscribe(std::string eventType, IListener* listener);
	void preTick(float dt, IActor* actor);
	void unSubscribe(std::string eventType, IListener* listener);
	std::string getType();
	void transition(std::string newState);
	virtual void tick(float dt, IActor* actor) = 0;
	virtual void setUp(IActor* actor) = 0;
	virtual void breakDown(IActor* actor) = 0;

protected:
	std::string type;
	Dispatcher* machines;
};

#endif