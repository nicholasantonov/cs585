#ifndef _SCENE_MANAGER_HH_
#define _SCENE_MANAGER_HH_

#include "scene-node.hh"
#include "dynamic-array.hh"
#include "i-tickable.hh"
#include "i-scene-graph.hh"
#include "i-renderer.hh"
#include "i-actor.hh"

class SceneManager
{
public:
	static SceneManager* getInstance();

	void tick(float dt);
	void render(float dt);

	void addTickable(ITickable* tickable);
	void addSceneNode(SceneNode* node);
	bool canMoveTo(SceneNode* node, int x, int y);
	void addActor(SceneNode* node);
	void setSceneGraph(ISceneGraph* graph);
	void setDefaultTile(SceneNode* node);
	void setRenderer(IRenderer* r);
	void moveCursor(int x, int y);
	int cursorX();
	int cursorY();
	void showCursor();
	void hideCursor();
	void moveCamera(int x, int y);
	int cameraX();
	int cameraY();
	void displayMessage(std::string message, float timeToDisplayFor);
	void displayMenu(DynamicArray<std::string>*);
	void closeMenu();
	void displayStatus(std::string);
	void closeStatus();
	IActor* getClosest(std::string type, int x, int y);
	SceneNode* getTopDrawableNodeAt(int x, int y);

	ISceneGraph* getSceneGraph();
	DynamicArray<ITickable*>* getTickables();
	DynamicArray<SceneNode*>* getActors();


	
private:
	SceneManager();

	SceneManager(SceneManager const&);
	void operator=(SceneManager const&);

	ISceneGraph* sceneGraph;
	DynamicArray<ITickable*>* tickables;
	DynamicArray<SceneNode*>* actors;
	IRenderer* renderer;
	int cursor_x;
	int cursor_y;
	int camera_x;
	int camera_y;
	
	static SceneManager* instance;
};

#endif