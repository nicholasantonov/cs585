#include "input-event.hh"

InputEvent::InputEvent(int key) :
	input(key)
{ }

std::string InputEvent::getType()
{
	return "input";
}

int InputEvent::UP = KEY_UP;
int InputEvent::DOWN = KEY_DOWN;
int InputEvent::LEFT = KEY_LEFT;
int InputEvent::RIGHT = KEY_RIGHT;