#include "json.hh"

#include "dynamic-array.hh"
#include "debug.hh"
#include "trie.hh"
#include <cstdlib>
#include <string>
#include <fstream>
#include <streambuf>
#include <sstream>

Json::Json()
{

}

Json* Json::parseFile(std::string fileName)
{
	DEBUG_LOG("json", "parsing file: " + fileName);
	Json* ret = new Json();

	std::ifstream inStream(fileName);

	std::string str;

	inStream.seekg(0, std::ios::end);
	str.reserve(inStream.tellg());
	inStream.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(inStream)), std::istreambuf_iterator<char>());

	ret->parse(str);

	return ret;
}

bool Json::parse(std::string string)
{
	for (unsigned int i = 0; i < string.length(); i++)
	{
		if (string[i] <= 32) // whitespace / non printable characters
		{
			continue;
		}

		else if (string[i] == '{')
		{
			parseObject(string, &i);
			type = OBJECT;
		}
		else if (string[i] == '[')
		{
			parseArray(string, &i);
			type = ARRAY;
		}
	}
	return true;
}

Json* Json::get(unsigned int index)
{
	if (type == ARRAY)
	{
		return arr->get(index);
	}
	DEBUG_ERROR("json", "getting as array from a non array");
	return new Json();
}

DynamicArray<Json*>* Json::getArray()
{
	if (type == ARRAY)
	{
		return arr;
	}
	DEBUG_ERROR("json", "getting as array from a non array");
	return nullptr;
}

float Json::getNumber()
{
	if (type == NUMBER)
	{
		return std::atof(dstring.c_str());
	}
	DEBUG_ERROR("json", "getting as number from a non number");
	return 0.0f;
}

std::string Json::getString()
{
	if (type == STRING)
	{
		return dstring;
	}
	DEBUG_ERROR("json", "getting as string from a non string");
	return dstring;
}

Json* Json::getMember(std::string string)
{
	if (type == OBJECT)
	{
		return members->get(string);
	}
	DEBUG_ERROR("json", "getting a member from a non object");
	return new Json();
}

std::string Json::parseString(std::string string, unsigned int* i)
{
	*i += 1; /// move to first char of string
	std::string ret = "";
	for (; string[*i] != '"'; *i += 1)
	{
		if (string[*i] == '\\')
		{
			*i += 1;
			switch (string[*i]) {
			case '"':
				ret.push_back('"');
				break;
			case '/':
				ret.push_back('/');
				break;
			case '\\':
				ret.push_back('\\');
				break;
			case 'b':
				ret.push_back('\b');
				break;
			case 'f':
				ret.push_back('\f');
				break;
			case 'n':
				ret.push_back('\n');
				break;
			case 'r':
				ret.push_back('\r');
				break;
			case 't':
				ret.push_back('\t');
				break;
			default:
				DEBUG_ERROR("json", "invalid '\\'");
				break;
			}
		}
		else
		{
			ret.push_back(string[*i]);
		}
	}
	*i += 1;
	return ret;
}

void Json::parseObject(std::string string, unsigned int* i)
{
	*i += 1;

	type = OBJECT;
	members = new Trie<Json*>();

	std::string currentName;
	Json* currentMember;

	while (*i < string.length() && string[*i] != '}')
	{
		if (string[*i] <= 32 || string[*i] == ',') // whitespace / non printable characters
		{
			*i += 1;
			continue;
		}
		currentName = getName(string, i);
		currentMember = parseValue(string, i);
		members->insert(currentName, currentMember);
		DEBUG_LOG("json", "inserting " + currentName);
	}

	*i += 1;
}

std::string Json::getName(std::string string, unsigned int* i)
{
	for (; string[*i] != '}'; *i += 1)
	{
		if (string[*i] <= 32) // whitespace / non printable characters
		{
			continue;
		}
		else if (string[*i] == '"')
		{
			return parseString(string, i);
		}
		else
		{
			std::stringstream ss;
			ss << (int)string[*i];
			std::string str = ss.str();
			DEBUG_ERROR("json", "Malformed object: expected \"name\", instead found " + str);
		}
	}
	DEBUG_ERROR("json", "Malformed object: Could Not Find Name");
	return "";
}

Json* Json::parseValue(std::string string, unsigned int* i)
{
	Json* item = new Json();
	for (; string[*i] != ',' && string[*i] != '}' && string[*i] != ']'; *i += 1)
	{
		if (string[*i] <= 32 || string[*i] == ':') // whitespace / non printable characters
		{
			continue;
		}

		else if (string[*i] == '{')
		{
			item->type = OBJECT;
			item->parseObject(string, i);
			break;
		}
		else if (string[*i] == '[')
		{
			item->type = ARRAY;
			item->arr = parseArray(string, i);
			break;
		}
		else if (string[*i] == '"')
		{
			item->type = STRING;
			item->dstring = parseString(string, i);
			break;
		}
		else if (string[*i] == 't')
		{
			*i += 4;
			item->type = BOOLEAN;
			item->boolean = true;
			break;
		}
		else if (string[*i] == 'f')
		{
			*i += 5;
			item->type = BOOLEAN;
			item->boolean = false;
			break;
		}
		else
		{
			item->dstring = parseNumber(string, i);
			item->type = NUMBER;
			break;
		}
	}
	return item;
}

std::string Json::parseNumber(std::string string, unsigned int* i)
{
	std::string ret = "";
	for (; string[*i] != ',' && string[*i] != ']' && string[*i] != '}'; *i += 1)
	{
		ret.push_back(string[*i]);
	}
	return ret;
}

DynamicArray<Json*>* Json::parseArray(std::string string, unsigned int* i)
{
	arr = new DynamicArray<Json*>();
	while (string[*i] != ']')
	{
		*i += 1;
		if (string[*i] > 32) // If not whitespace
		{
			arr->push(parseValue(string, i));
		}
	}
	if (arr->length())
	{
		arr->remove(arr->length() - 1);
	}
	return arr;
}