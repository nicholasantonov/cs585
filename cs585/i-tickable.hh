#ifndef _I_TICKABLE_HH_
#define _I_TICKABLE_HH_

class ITickable
{
public:
	virtual void tick(float dt) = 0;
};

#endif