#ifndef _CHARACTER_HH_
#define _CHARACTER_HH_

#define CHARACTER_LEVEL 5
#define CHARACTER_COLLISION ((unsigned long)0b10)
#define DWARF_TEAM 1
#define ORC_TEAM 2
#define NO_WEAPON 0
#define LESSER_WEAPON 1
#define NORMAL_WEAPON 2
#define MASTERWORK_WEAPON 3
#define NO_POTION 0
#define LESSER_POTION 1
#define NORMAL_POTION 2
#define MASTERWORK_POTION 3

#include "scene-node.hh"
#include "trie.hh"
#include "dispatcher.hh"
#include "i-actor.hh"
#include "building.hh"

class Character : public IActor
{
public:
	Character();
	Character(int xPos, int yPos, unsigned long collisionMap, IDrawable* d, float hlth);
	~Character();

	void addListener(std::string eventType, IListener* listener);
	void removeListener(std::string eventType, IListener* listener);
	void handleInput(int input, int inputX, int inputY);
	bool canBeInteractedWith();
	std::string getType();

	void dealDamage(float damage, Character* attacker);
	std::string getDescription();

	void simulateNeeds(float dt);

	const float maxHealth;
	float health;
	Dispatcher* dispatcher;
	Building* currentBuilding;
	int id;
	Character* target;
	std::string type;
	bool isMoveable;
};

#endif
