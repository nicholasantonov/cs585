#include "attack-state.hh"

#include "scene-manager.hh"
#include "character.hh"
#include "i-actor.hh"
#include <cstdlib>

AttackState::AttackState()
{

}

void AttackState::tick(float dt, IActor* a)
{
	Character* actor = (Character*)a;
	actor->simulateNeeds(dt);
	actor->attributes->get("timesincelastattack") += dt;
	if (actor->attributes->get("timesincelastattack") > actor->attributes->get("attackdelay"))
	{
		actor->attributes->get("timesincelastattack") -= actor->attributes->get("attackdelay");

		for (int x = actor->getX() - 1; x <= actor->getX() + 1; x++)
		{
			for (int y = actor->getY() - 1; y <= actor->getY() + 1; y++)
			{
				if (actor->target == SceneManager::getInstance()->getTopDrawableNodeAt(x, y))
				{
					attackWith(actor);
					return;
				}
			}
		}

		// If this is executed, target ran away
		actor->target = nullptr;
		transition("idle");
	}
	else if ((actor->maxHealth - actor->health) > (10 * actor->attributes->get("potion")))
	{
		actor->health += (10 * actor->attributes->get("potion"));
		actor->attributes->insert("potion", NO_POTION);
	}
}

void AttackState::setUp(IActor* actor)
{

}

void AttackState::breakDown(IActor* actor)
{

}

void AttackState::attackWith(Character* actor)
{
	if ((rand() % 100) < actor->attributes->get("hitchance"))
	{
		actor->target->dealDamage(10 + (10 * actor->attributes->get("weapon")), actor);
	}
}