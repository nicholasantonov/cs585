#ifndef _GENERIC_EVENT_HH
#define _GENERIC_EVENT_HH

#include "i-event.hh"

#include <string>

// Generic event class for creating events with just a type but no other properties
class GenericEvent : public IEvent
{
public:
	GenericEvent();
	GenericEvent(std::string);
	std::string getType();
private:
	std::string type;
};

#endif