#include "scene-manager.hh"

#include "dynamic-array.hh"
#include "i-tickable.hh"
#include "fixed-grid.hh"
#include "i-actor.hh"
#include <cmath>

#define INT_MAX 2147483647

SceneManager* SceneManager::instance = nullptr;

SceneManager* SceneManager::getInstance()
{
	if (instance == nullptr)
	{
		instance = new SceneManager();
	}
	return instance;
}

void SceneManager::tick(float dt)
{
	for (unsigned int i = 0; i < tickables->length(); i++)
	{
		tickables->get(i)->tick(dt);
	}
}

void SceneManager::render(float dt)
{
	renderer->tick(dt);
}

void SceneManager::moveCursor(int x, int y)
{
	cursor_x = x;
	cursor_y = y;
	renderer->moveCursor(x, y);
}

void SceneManager::moveCamera(int x, int y)
{
	camera_x = x;
	camera_y = y;
	renderer->setCamera(x, y);
}

int SceneManager::cameraX()
{
	return camera_x;
}

int SceneManager::cameraY()
{
	return camera_y;
}

void SceneManager::showCursor()
{
	renderer->showCursor();
}

void SceneManager::hideCursor()
{
	renderer->hideCursor();
}

void SceneManager::addTickable(ITickable* tickable)
{
	tickables->push(tickable);
}

void SceneManager::addSceneNode(SceneNode* node)
{
	sceneGraph->addSceneNode(node);
}

void SceneManager::addActor(SceneNode* node)
{
	actors->push(node);
}

void SceneManager::setRenderer(IRenderer* r)
{
	if (renderer != nullptr)
	{
		delete renderer;
	}
	renderer = r;
}

ISceneGraph* SceneManager::getSceneGraph()
{
	return sceneGraph;
}

DynamicArray<ITickable*>* SceneManager::getTickables()
{
	return tickables;
}

DynamicArray<SceneNode*>* SceneManager::getActors()
{
	return actors;
}

void SceneManager::setSceneGraph(ISceneGraph* graph)
{
	if (sceneGraph != nullptr)
	{
		delete sceneGraph;
	}
	sceneGraph = graph;
}

void SceneManager::setDefaultTile(SceneNode* node)
{
	sceneGraph->setDefaultTile(node);
}

SceneNode* SceneManager::getTopDrawableNodeAt(int x, int y)
{
	return sceneGraph->getTopDrawableNodeAt(x, y);
}

int SceneManager::cursorX()
{
	return cursor_x;
}

int SceneManager::cursorY()
{
	return cursor_y;
}

void SceneManager::displayMessage(std::string message, float timeToDisplayFor)
{
	renderer->displayMessage(message, timeToDisplayFor);
}

void SceneManager::displayMenu(DynamicArray<std::string>* items)
{
	renderer->displayMenu(items);
}

void SceneManager::closeMenu()
{
	renderer->closeMenu();
}

void SceneManager::displayStatus(std::string status)
{
	renderer->displayStatus(status);
}

void SceneManager::closeStatus()
{
	renderer->closeStatus();
}

IActor* SceneManager::getClosest(std::string type, int x, int y)
{
	IActor* best = nullptr;
	int distance = INT_MAX;
	for (unsigned int i = 0; i < actors->length(); i++)
	{
		IActor* actor = (IActor*)actors->get(i);
		if (actor->getType().compare(type) == 0)
		{
			int dx = std::abs(actor->getX() - x);
			int dy = std::abs(actor->getY() - y);

			int d = (dx * dx) + (dy * dy);

			if (d < distance)
			{
				distance = d;
				best = actor;
			}
		}
	}
	return best;
}

// Returns if the specified scenenode can move to another node without a collision
bool SceneManager::canMoveTo(SceneNode* node, int x, int y)
{
	return sceneGraph->canMoveTo(node, x, y);
}

SceneManager::SceneManager() : 
	sceneGraph(nullptr),
	tickables(new DynamicArray<ITickable*>),
	actors(new DynamicArray<SceneNode*>),
	renderer(nullptr),
	cursor_x(0),
	cursor_y(0),
	camera_x(0),
	camera_y(0)
{
	
}