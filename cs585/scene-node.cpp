#include "scene-node.hh"

SceneNode::SceneNode() :
x(0),
y(0),
map(0)
{}

SceneNode::SceneNode(int xPos, int yPos) :
x(xPos),
y(yPos),
map(0)
{}

SceneNode::SceneNode(int xPos, int yPos, unsigned long collisionMap, IDrawable* d) :
drawable(d),
x(xPos),
y(yPos),
map(collisionMap)
{}

void SceneNode::update(int xPos, int yPos)
{
	x = xPos;
	y = yPos;
}

unsigned long SceneNode::collisions()
{
	return map;
}

int SceneNode::getX()
{
	return x;
}

int SceneNode::getY()
{
	return y;
}