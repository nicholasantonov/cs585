#ifndef _CAMERA_STATE_HH_
#define _CAMERA_STATE_HH_

#include "i-state.hh"

class CameraState : public IState
{
public:
	CameraState();

	void tick(float dt, IActor* actor);
	void setUp(IActor* actor);
	void breakDown(IActor* actor);

	class OnKey : public IListener
	{
	public:
		IState* instance;
		void handle(IEvent* e);
	} _onKey;
};

#endif