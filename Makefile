NAME = game
LDFLAGS += -lmenu -lncurses
CXXFLAGS += -Wall -Werror -std=c++11 -g
TARBALL = nantonov-$(NAME).tar.gz
SOURCES = cs585/main.cpp cs585/ascii-drawable.cpp cs585/ascii-renderer.cpp \
cs585/attack-state.cpp \
cs585/actor-factory.cpp cs585/brownian-state.cpp \
cs585/building.cpp cs585/building-node.cpp cs585/building-controller.cpp \
cs585/character.cpp \
cs585/cursor-state.cpp cs585/camera-state.cpp \
cs585/debug.cpp cs585/test.cpp  cs585/death-event.cpp \
cs585/dispatcher.cpp \
cs585/display-info.cpp \
cs585/event-bus.cpp cs585/fixed-grid.cpp cs585/game.cpp cs585/gnomelevel.cpp \
cs585/interact-state.cpp cs585/input-manager.cpp cs585/input-event.cpp \
cs585/json.cpp cs585/level-manager.cpp \
cs585/move-state.cpp cs585/ncurses-input.cpp \
cs585/orc-spawner.cpp cs585/scene-manager.cpp \
cs585/grand-hall-function.cpp \
cs585/generic-event.cpp cs585/i-state.cpp cs585/state-machine.cpp \
cs585/leveled-building-function.cpp \
cs585/state-transition.cpp cs585/scene-node.cpp \
cs585/terrain.cpp cs585/to-string.cpp
OBJECTS = $(SOURCES:.cpp=.o)
RM = rm -rf
DIR = cs585/
CC = g++


all: $(OBJECTS)
	$(CC) $(CXXFLAGS) $(OBJECTS) $(LDFLAGS) -g -o $(NAME)

clean:
	-$(RM) $(DIR)*~
	-$(RM) $(DIR)\#*
	-$(RM) $(DIR)*.o
	-$(RM) $(DIR)*.core
	-$(RM) $(DIR)*.exe

fclean:	clean
	-$(RM) $(DIR)$(NAME)

re: fclean all

test: all
	./$(NAME) test

retest: re test