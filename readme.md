# Coding Standards

A strict and logical coding standard makes code far easier to read an understand quickly. For this reason, I am outlining several practices that I will follow throughout my projects in order to keep my code understandable and maintainable. 

## Indentation

All indentation is to be done using tabs, with one tab per level of scope.

The ```public```, ```private```, and ```protected``` tags inside classes are not to be indented.

## Code Blocks

Each opening and closing brace will be on it's own line, even for blocks of only 1 line after an ```if``` or a ```for```

### If statements

If statements will have a space between ```if``` and ```(``` to further differentiate it from looking like a function call.

### for loops

For loops will have a space after each of the two ```;``` inside of it, but not before. 

For loops will have a space between ```for``` and ```(``` to further differentiate it from looking like a function call.

### While loops

Conditionals after a while will have a space between ```while``` and ```(``` to further differentiate it from looking like a function call.

## Spacing

Any sets of parenthesis will have no spacing on the inside between the parenthesis and the code it contains

Function calls are to have no space between the name and the parentesis. Conditionals in ```if```, ```for```, and ```while``` loops should have a space before the first parentesis.

Infix operators should be surrounded by spaces, except those with higher priority such as ```,```

## Line Limitations

There will be a maximum of one statement per line, ending in a semicolon.

If a statement must be broken into multiple lines, it will be broken after a ```,``` or a binary operator

## Pointers

When declaring a pointer, the ```*``` will be adjacent to the type, not the variable name.

## Capitalization

All variable and function names will use camelCase

All class names will use PascalCase